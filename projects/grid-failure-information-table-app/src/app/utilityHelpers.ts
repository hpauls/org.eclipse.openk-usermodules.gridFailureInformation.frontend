/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export function dateTimeComparator(filterLocalDateAtMidnight: Date, cellValue: string): number {
  if (cellValue == null) return -1;
  const cellDate: Date = new Date(cellValue.replace('Z', ''));
  cellDate.setHours(0, 0, 0, 0);

  const filterLocalDateAtMidnightTimeStamp = filterLocalDateAtMidnight.getTime();
  const cellDateTimeStamp = cellDate.getTime();

  if (filterLocalDateAtMidnightTimeStamp === cellDateTimeStamp) {
    return 0;
  }
  if (cellDateTimeStamp < filterLocalDateAtMidnightTimeStamp) {
    return -1;
  }
  if (cellDateTimeStamp > filterLocalDateAtMidnightTimeStamp) {
    return 1;
  }
}

export const stringInsensitiveComparator = (firstValue: string, secondValue: string) => {
  firstValue = firstValue ? firstValue : '';
  secondValue = secondValue ? secondValue : '';

  return firstValue.toLowerCase().localeCompare(secondValue.toLowerCase());
};
