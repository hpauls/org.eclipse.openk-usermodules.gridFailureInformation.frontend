<h1>HOW-TO Run<h1>

## Installation

```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
# $ npm run build (muss vor prod gelaufen sein)
$ npm run start:prod
```

## Important REST URLS

## Local:

### POST URL

http://localhost:3000/internal-sit

### GET URL

http://localhost:3000/public-sit

## DEV Env:

### POST URL

http://entdockergss:3003/internal-sit

### GET URL

http://entdockergss:3003/public-sit
