= openKonsequenz - How to build the module "eLogbook@openK"
:Date: 2017-11-17
:Revision: 1
:icons:
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

<<<

IMPORTANT: Please be sure that you have first *Portal (Auth n Auth)* installed and configured!

== Requirements
* Apache Tomcat 8.0.30
* Node.js 6.10.0
* Angular-CLI
* Graphviz

== How to build the Backend
To build the backend you need to have installed Apache Maven and Java (look at `/deploy/`*get_started.txt*).

=== Install and Configure Apache Tomcat
Tomcat is an open-source Java Servlet Container and provides a "pure Java" HTTP web server environment in which Java code can run.

* Download Tomcat version 8.0.30 from https://archive.apache.org/dist/tomcat/tomcat-8/v8.0.30/bin/
and extract it (apache-tomcat-8-0-30.zip).
* Place the extracted folder on your C drive.
* Configure Tomcat:
** Configure the *context.xml* file. Copy the *<Resource … >* block from the project file `deploy/conf/`*context.xml*
and paste it in the `<tomcat>/conf/`*context.xml* in the *<Context>* block.
** To run both backend projects (elogbook and portal) at the same time on Tomcat you have to
 give him the parameters for different backend configs. Just paste the following line in the *context.xml* over the *<Resource … >*:

 <Parameter name="OK_ELOGBOOK_ENVIRONMENT" override="false" value="DevLocal"/>

** Dont forget to save it.


TIP: For more informations check *Deployment View* Chapter in *http://87.128.212.254:8880/elogbookFE_ADoc/elogbook_architectureDocumentation.html[elogbook_architectureDocumentation]*.

** Tomcat also needs the database driver to be placed in `<tomcat>/lib`. Find it in the project file `deploy/lib/`*postgresql-xxx.jar*.


=== Install Graphviz
Graphviz is open source graph visualization software. You need this software because of auto-generated graphics in the documentations.

* Download and install Graphviz from http://www.graphviz.org/Download_windows.php
* If not, set the PATH variable

 Name of variable: GRAPHVIZ_DOT
 Value of variable: <GRAPHVIZ_DIR>\bin\dot.exe


== How to build the Frontend

=== Install Node.js

Node.js is an open-source, cross-platform JavaScript run-time environment for executing JavaScript (also Typescript which is a strongly typed form of JavaScript) code server-side.

* Download Node.js 6.10.0 from https://nodejs.org/download/release/v6.10.0/ by clicking on a xxx.msi image for windows.
* Run the downloaded Node.js .msi Installer - including accepting the license, selecting the destination, and authenticating for the install.
This requires Administrator privileges, and you may need to authenticate
* To ensure Node.js has been installed, run the following in your terminal - you should get something like v6.10.0

[source,command]
----
    $ node -v
----
{blank}

=== Update the npm Angular-CLI client
This package manager comes bundled with Node.js and its good to have the latest version.

* Open a command line and navigate to the root folder of the frontend project
* Run the following commands (the version is defined in the package.json file):

[source,command]
----
    $ npm install --save-dev @angular/cli
----
{blank}

[source,command]
----
    $ npm install
----
{blank}

TIP: Next Step -> Read the *elogbook_howtoRun* file!



