/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
// Angular core modules
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule, APP_BASE_HREF, PlatformLocation } from '@angular/common';

// Routes
import { AppRoutingModule } from '@grid-failure-information-app/app/app-routing.module';
// Modules
import { AppComponent } from '@grid-failure-information-app/app/app.component';
import { HttpServiceModule } from '@grid-failure-information-app/shared/async-services/http/http.module';
import { UtilityModule } from '@grid-failure-information-app/shared/utility';

//Feature Modules
import { GridFailureModule } from '@grid-failure-information-app/pages/grid-failure/grid-failure.module';
import { LogoutModule } from '@grid-failure-information-app/pages/logout/logout.module';
import { DistributionGroupModule } from '@grid-failure-information-app/pages/distribution-group/distribution-group.module';

// Store
import { reducers } from '@grid-failure-information-app/shared/store';
import { SettingsEffects } from '@grid-failure-information-app/shared/store/effects/settings.effect';

// Guards
import { PublisherGuard } from '@grid-failure-information-app/shared/guards/publisher.guard';

// Services
import { ConfigService } from './app-config.service';

// Third party libraries

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { environment } from '@grid-failure-information-app/environments/environment';
import { ContainersModule } from '@grid-failure-information-app/shared/containers/containers.module';
import { NgbModalModule } from '@ng-bootstrap/ng-bootstrap';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { AppConfigApiClient } from '@grid-failure-information-app/app/app-config-api-client';

import { AgGridModule} from 'ag-grid-angular';

/**
 * Get the BaseHref which is defined in the index.html ( <base href="/"> ).
 * The "base href" in the index.html can be set via ng nuild for different enviroments.
 *
 * */

export function getBaseHref(platformLocation: PlatformLocation): string {
  return platformLocation.getBaseHrefFromDOM();
}

/**
 * Calling functions or calling new is not supported in metadata when using AoT.
 * The work-around is to introduce an exported function.
 *
 * The reason for this limitation is that the AoT compiler needs to generate the code that calls the factory
 * and there is no way to import a lambda from a module, you can only import an exported symbol.
 */

export function configServiceFactory(config: ConfigService) {
  return () => config.load();
}
@NgModule({
  declarations: [AppComponent],
  imports: [
    // Angular core dependencies
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CommonModule,
    // Third party modules
    SimpleNotificationsModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (http: HttpClient) => new TranslateHttpLoader(http, 'i18n/', '.json'),
        deps: [HttpClient],
      },
    }),
    // App custom dependencies
    HttpServiceModule.forRoot(),
    UtilityModule.forRoot(),
    EffectsModule.forRoot([SettingsEffects]),
    StoreModule.forRoot(reducers),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    AgGridModule,
    ContainersModule,
    GridFailureModule,
    AppRoutingModule,
    NgbModalModule,
    LogoutModule,
    DistributionGroupModule,
  ],
  providers: [
    PublisherGuard,
    AppConfigApiClient,
    ConfigService,
    {
      provide: APP_INITIALIZER,
      useFactory: configServiceFactory,
      deps: [ConfigService],
      multi: true,
    },
    {
      provide: APP_BASE_HREF,
      useFactory: getBaseHref,
      deps: [PlatformLocation],
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
