/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async } from '@angular/core/testing';
import { Router } from '@angular/router';
import { GridFailureDetailsSandbox } from '@grid-failure-information-app/pages/grid-failure/grid-failure-details/grid-failure-details.sandbox';
import { StateEnum } from '@grid-failure-information-app/shared/constants/enums';
import {
  DistributionGroup,
  FailureBranch,
  FailureHousenumber,
  FailureStation,
  GridFailure,
  PublicationChannel,
} from '@grid-failure-information-app/shared/models';
import { State } from '@grid-failure-information-app/shared/store';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import * as preConfigurationActions from '@grid-failure-information-app/shared/store/actions/settings.action';
import { FORM_ID, GAS_BRANCH_ID_KEY, INITIAL_STATE } from '@grid-failure-information-app/shared/store/reducers/grid-failures/grid-failure-details-form.reducer';
import { UtilService } from '@grid-failure-information-app/shared/utility/utility.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ActionsSubject, Store } from '@ngrx/store';
import { DisableAction, EnableAction, SetUserDefinedPropertyAction, box } from 'ngrx-forms';
import { BehaviorSubject, of } from 'rxjs';

describe('GridFailureDetailsSandbox', () => {
  let service: GridFailureDetailsSandbox;
  let appState: Store<State>;
  let actionSubject: ActionsSubject;
  let utilService: UtilService;
  let router: Router;
  let modalService: NgbModal;
  let dispatchSpy: any;
  let configService: any;
  let externalApiClient: any;

  beforeEach(async(() => {
    appState = { dispatch: () => {}, pipe: () => of(true), select: () => of(true), userDefinedProperties: { controlId: '' } } as any;
    actionSubject = { pipe: () => of(true), map: () => ({}), next: () => ({}) } as any;
    utilService = { displayNotification() {} } as any;
    router = { navigateByUrl() {} } as any;
    modalService = { open() {} } as any;
    dispatchSpy = spyOn(appState, 'dispatch').and.callFake(() => {});
    const branch: FailureBranch = new FailureBranch();
    branch.id = '00';
    branch.name = 'OS';
    branch.description = 'ohne Sparte';
    configService = { getEnv: () => 'test' } as any;
    externalApiClient = { getNominatimData: () => {} };
    service = new GridFailureDetailsSandbox(appState, actionSubject, router, utilService, modalService, configService, externalApiClient);
    service.currentFormState = INITIAL_STATE;
    service.gridFailureBranches$ = of([branch]);
    service.gridFailureDetailsFormState$ = of((INITIAL_STATE.value.branchId = '00' as any));
    service.noBranchId = '123';
    service.addressPostCodes$ = of(['test', 'test1', 'test2', 'hello']);
    service.gridFailurePublicationChannels$ = of([new PublicationChannel()]);
    const station = new FailureStation();
    station.latitude = 1;
    station.longitude = 1;
    service.gridFailureStations$ = of([station]);
  }));

  it('should create GridFailureDetailsSandbox service ', () => {
    expect(service).toBeTruthy();
  });

  xit('should loadStations via init()', () => {
    const spy = spyOn(service as any, 'setBranchIds');
    service.gridFailureStations$ = of([new FailureStation()]);
    service.init();
    expect(spy).toHaveBeenCalled();
  });

  it('should not calculatePolygon via init() if no stations are provided', () => {
    const spy = spyOn(service as any, 'setBranchIds');
    const spy2 = spyOn(service as any, '_calculatePolygon');
    service.gridFailureStations$ = of([]);
    service.init();
    expect(spy).toHaveBeenCalled();
    expect(spy2).not.toHaveBeenCalled();
  });

  it('should dispatch loadGridFailure Action via loadGridFailure(id)', () => {
    service.loadGridFailure('id');
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureDetail({ payload: 'id' }));
  });

  it('should dispatch loadGridFailureBranches Action via loadGridFailureBranches()', () => {
    service.loadGridFailureBranches();
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureBranches());
  });

  it('should dispatch loadGridFailureClassifications Action via loadGridFailureClassifications()', () => {
    service.loadGridFailureClassifications();
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureClassifications());
  });

  it('should dispatch loadGridFailureStates Action via loadGridFailureStates()', () => {
    service.loadGridFailureStates();
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureStates());
  });

  it('should dispatch loadGridFailureRadii Action via loadGridFailureRadii()', () => {
    service.loadGridFailureRadii();
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureRadii());
  });

  xit('should dispatch loadGridFailureExpectedReasons Action via registerLoadGridFailureExpectedReasons()', () => {
    const branch: FailureBranch = new FailureBranch();
    branch.id = '00';
    branch.name = 'OS';
    branch.description = 'ohne Sparte';
    const branches: FailureBranch[] = [branch];
    service.gridFailureDetailsFormState$ = of((INITIAL_STATE.value.branchId = '00' as any));
    service.currentFormState = INITIAL_STATE.value.branchId = '00' as any;
    service.gridFailureBranches$ = of(branches);
    (service as any)._registerLoadGridFailureExpectedReasons();

    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureExpectedReasons({ payload: branch.name }));
  });

  it('should dispatch loadGridFailureVersions Action via loadGridFailureVersions(id)', () => {
    service.loadGridFailureVersions('id');
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureVersions({ payload: 'id' }));
  });

  it('should set oldVersion true if versionNumber < maxVersionNumber', () => {
    service['gridFailureId'] = 'id';
    service.maxVersionNumber = 2;
    service.loadGridFailureVersion('1');
    expect(service.oldVersion).toBeTruthy();
  });

  it('should dispatch loadGridFailureDetail action if versionNumber ===  maxVersionNumber', () => {
    service['gridFailureId'] = 'id';
    service.maxVersionNumber = 1;
    service.loadGridFailureVersion('1');
    expect(dispatchSpy).toHaveBeenCalledTimes(4);
    expect(dispatchSpy.calls.all()[0].args[0].type).toEqual('[GridFailure Details] Load');
  });

  it('should dispatch loadGridFailureVersion Action via loadGridFailureVersion(VsNo)', () => {
    service['gridFailureId'] = 'id';
    service.maxVersionNumber = 0;
    service.loadGridFailureVersion('1');
    expect(appState.dispatch).toHaveBeenCalledTimes(4);
  });

  it('should open modal dialog when deleteGridFailure() called', () => {
    spyOn(service['_modalService'], 'open').and.returnValue({ componentInstance: { title: 'xx' }, result: { then: () => of(true) } } as any);
    service.deleteGridFailure();
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should clear form state when current change is canceled and form state is pristine', () => {
    const spy: any = spyOn(service as any, '_clear').and.callThrough();
    service.currentFormState = { isPristine: true } as any;
    service.cancel();
    expect(spy).toHaveBeenCalled();
  });

  it('should open modal when current change is canceled and form state is not pristine', () => {
    spyOn(service['_modalService'], 'open').and.returnValue({ componentInstance: { title: '' }, result: { then: () => of(true) } } as any);
    service.currentFormState = { isPristine: false } as any;
    service.cancel();
    expect(modalService.open).toHaveBeenCalled();
  });

  it('should dispatch actions to clear the form', () => {
    service['_clear']();
    expect(appState.dispatch).toHaveBeenCalledTimes(3);
  });

  it('should call dispatch and clear if save a valid grid failure detail', () => {
    const spy1 = spyOn(service, 'setFormStatePristine');
    const gfdetail = new GridFailure();
    gfdetail.id = 'id';
    gfdetail.responsibility = 'test';
    gfdetail.postcode = '00000';
    service['_assignedDistributionGroups'] = [];

    service.currentFormState = INITIAL_STATE;
    (service.currentFormState as any).isValid = true;
    (service.currentFormState as any).value = gfdetail;
    (service.currentFormState as any).userDefinedProperties = { controlId: '' };

    service.saveGridFailure();
    expect(dispatchSpy).toHaveBeenCalledWith(
      Object({ payload: { gridFailure: gfdetail, saveForPublish: false }, type: gridFailureActions.saveGridFailure.type })
    );
    expect(spy1).toHaveBeenCalled();
  });

  it('should display a notification if the grid failure detail is invalid', () => {
    const spy1 = spyOn(utilService, 'displayNotification');
    const gfdetail = new GridFailure();
    gfdetail.id = 'id';
    gfdetail.responsibility = 'test';
    gfdetail.postcode = '00000';

    service.currentFormState = INITIAL_STATE;
    (service.currentFormState as any).isValid = false;
    (service.currentFormState as any).value = gfdetail;
    (service.currentFormState as any).userDefinedProperties = { controlId: '' };

    service.saveGridFailure();
    expect(spy1).toHaveBeenCalled();
  });

  it('should register events', () => {
    const gridFailure: GridFailure = new GridFailure();
    gridFailure.versionNumber = 3;
    const gridFailureVersions: GridFailure[] = [gridFailure];
    spyOn(actionSubject, 'pipe').and.returnValue(of({ payload: gridFailureVersions }));
    const spy = spyOn(service as any, '_showButtonsByState');

    service.gridFailureDetailsFormState$ = of({ ...INITIAL_STATE });
    service.registerEvents();
    expect(service.currentFormState).toBeDefined();
    expect(spy).toHaveBeenCalled();
  });

  it('should dispatch action in response to controlId = gridFailureDetailsForm', () => {
    let formState: any = {
      id: 'gridFailureDetailsForm',
      value: {
        statusIntern: StateEnum.CREATED,
        postcode: '00000',
      },
      userDefinedProperties: { controlId: 'gridFailureDetailsForm' },
      controls: {
        postcode: { id: 'gridFailureDetailsForm.postcode' },
      },
    };

    service.gridFailureDetailsFormState$ = of({ ...INITIAL_STATE, formState });
    service.registerEvents();
    expect(appState.dispatch).toHaveBeenCalled();
  });

  it('should show qualify button state is applied', () => {
    const state = StateEnum.CREATED;
    (service as any)._showButtonsByState(state);
    expect(service.showQualifyButton).toBeTruthy();
  });

  it('should show qualify button state is updated', () => {
    const state = StateEnum.UPDATED;
    (service as any)._showButtonsByState(state);
    expect(service.showQualifyButton).toBeTruthy();
  });

  it('should show storno button state is applied', () => {
    const state = StateEnum.CREATED;
    (service as any)._showButtonsByState(state);
    expect(service.showStornoButton).toBeTruthy();
  });

  it('should show storno button state is updated', () => {
    const state = StateEnum.UPDATED;
    (service as any)._showButtonsByState(state);
    expect(service.showStornoButton).toBeTruthy();
  });

  it('should not show storno button or qualify button if other state', () => {
    const state = StateEnum.COMPLETED;
    (service as any)._showButtonsByState(state);
    expect(service.showQualifyButton).toBeFalsy();
    expect(service.showStornoButton).toBeFalsy();
  });

  it('should show update button if qualified', () => {
    let state = StateEnum.QUALIFIED;
    (service as any)._showButtonsByState(state);
    expect(service.showUpdateButton).toBeTruthy();
  });

  it('should show create button if state is NEW', () => {
    const state = StateEnum.NEW;
    (service as any)._showButtonsByState(state);
    expect(service.showCreatedButton).toBeTruthy();
  });

  it('should show create button if state is PLANNED', () => {
    const state = StateEnum.PLANNED;
    (service as any)._showButtonsByState(state);
    expect(service.showCreatedButton).toBeTruthy();
    expect(service.showDeleteButton).toBeTruthy();
  });

  it('should not show state buttons if state is an empty string', () => {
    (service as any)._showButtonsByState('');
    expect(service.showCreatedButton).toBeFalsy();
    expect(service.showStornoButton).toBeFalsy();
    expect(service.showQualifyButton).toBeFalsy();
  });

  it('should set the state in form to qualify and call save fuction', () => {
    service.gridFailureStates$ = { pipe: () => of({}), map: () => of({}) } as any;
    spyOn(service.gridFailureStates$, 'pipe').and.returnValue(of([{ status: StateEnum.QUALIFIED, id: '123' }]));
    const spySave = spyOn(service, 'saveGridFailure');
    service.setState(StateEnum.QUALIFIED);
    expect(spySave).toHaveBeenCalled();
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should set the state in form to canceled and call save fuction', () => {
    service.gridFailureStates$ = { pipe: () => of({}), map: () => of({}) } as any;
    spyOn(service.gridFailureStates$, 'pipe').and.returnValue(of([{ status: StateEnum.CANCELED, id: '123' }]));
    const spySave = spyOn(service, 'saveGridFailure');
    service.setState(StateEnum.CANCELED);
    expect(spySave).toHaveBeenCalled();
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should set the state in form to qualify and call save fuction', () => {
    service.gridFailureStates$ = { pipe: () => of({}), map: () => of({}) } as any;
    spyOn(service.gridFailureStates$, 'pipe').and.returnValue(of([{ status: StateEnum.QUALIFIED, id: '123' }]));
    const spySave = spyOn(service, 'saveGridFailure');
    service.setState(StateEnum.QUALIFIED);
    expect(spySave).toHaveBeenCalled();
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should set the state in form to canceled and call save fuction', () => {
    service.gridFailureStates$ = { pipe: () => of({}), map: () => of({}) } as any;
    spyOn(service.gridFailureStates$, 'pipe').and.returnValue(of([{ status: StateEnum.CANCELED, id: '123' }]));
    const spySave = spyOn(service, 'saveGridFailure');
    service.setState(StateEnum.CANCELED);
    expect(spySave).toHaveBeenCalled();
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should set the state in form to updated and call save fuction', () => {
    service.gridFailureStates$ = { pipe: () => of({}), map: () => of({}) } as any;
    spyOn(service.gridFailureStates$, 'pipe').and.returnValue(of([{ status: StateEnum.UPDATED, id: '123' }]));
    const spySave = spyOn(service, 'saveGridFailure');
    service.setState(StateEnum.UPDATED);
    expect(spySave).toHaveBeenCalled();
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should load GridFailureStations', () => {
    service.loadStations();
    expect(dispatchSpy).toHaveBeenCalledWith(Object({ type: gridFailureActions.loadStations.type }));
  });

  it('should map lon and lat values', () => {
    const gfdetail = new GridFailure();
    gfdetail.id = 'id';
    gfdetail.responsibility = 'test';

    service.gridFailureDetailsFormState$ = of({ isValid: true, value: gfdetail } as any);

    service.latLonMapping(null, false);
    expect(dispatchSpy).not.toHaveBeenCalled();

    const event = new FailureStation();
    event.latitude = 1.24;
    event.longitude = 1.23;

    service.latLonMapping(event, false);

    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should dispatch loadAddressPostalcodes Action via loadAddressPostalcodes()', () => {
    service.loadAddressPostalcodes();
    const postalCodes = gridFailureActions.loadAddressPostalcodes({ branch: null, community: null, district: null });
    expect(appState.dispatch).toHaveBeenCalledWith(postalCodes);
  });

  it('should map stationId to gridFailure object', () => {
    const gfdetail = new GridFailure();
    gfdetail.id = 'id';
    gfdetail.stationId = '1';

    service.currentFormState = INITIAL_STATE;
    (service.currentFormState as any).isValid = true;
    (service.currentFormState as any).value = gfdetail;

    const stationId = '2';

    service.setStationId(stationId);

    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should reset Coords', () => {
    const gfdetail = new GridFailure();
    gfdetail.id = 'id';
    gfdetail.longitude = 1.0;
    gfdetail.latitude = 1.0;

    service.currentFormState = INITIAL_STATE;
    (service.currentFormState as any).isValid = true;
    (service.currentFormState as any).value = gfdetail;

    service.resetCoords();

    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should reset stationId', () => {
    const gfdetail = new GridFailure();
    gfdetail.id = 'id';
    gfdetail.stationId = 'test';
    service.currentFormState = INITIAL_STATE;
    (service.currentFormState as any).isValid = true;
    (service.currentFormState as any).value = gfdetail;

    service.resetStationId();

    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should trigger formatter and check if the right value was returned', () => {
    const failureStation: FailureStation = new FailureStation();
    failureStation.stationName = 'test';
    failureStation.stationId = 'test';
    const result1 = service.formatter(failureStation);
    const result2 = service.formatter('test');

    expect(result1).toBe('test (test)');
    expect(result2).toBe('test');
  });

  it('should dispatch Action after loadPostalCodes', () => {
    service.loadAddressPostalcodes();
    const postalCodes = gridFailureActions.loadAddressPostalcodes({ branch: null, community: null, district: null });
    expect(appState.dispatch).toHaveBeenCalledWith(postalCodes);
  });

  it('should trigger searchForStation  and check if the right value was returned', () => {
    const failureStation: FailureStation = new FailureStation();
    failureStation.stationName = 'hello';
    (service as any)._stations = [new FailureStation(), new FailureStation(), failureStation];
    const text$ = of('hello');
    const result = service.searchForStation(text$);

    result.subscribe(item => {
      expect(item).toEqual([failureStation]);
    });
  });

  it('should trigger searchForStation  and check if the right value was returned if length is under 2', () => {
    const failureStation: FailureStation = new FailureStation();
    failureStation.stationName = 'hello';
    (service as any)._stations = [new FailureStation(), new FailureStation(), failureStation];
    const text$ = of('h');
    const result = service.searchForStation(text$);

    result.subscribe(item => {
      expect(item).toEqual([]);
    });
  });

  it('should call setLatLong and dispatch loadGridFailureAddress()', () => {
    const failureHousenumber = new FailureHousenumber();
    failureHousenumber.housenumber = 'test';
    failureHousenumber.uuid = 'hello';
    service['_addressHouseNumbers$'] = new BehaviorSubject<FailureHousenumber[]>([]);
    service['_addressHouseNumbers$'].next([failureHousenumber]);
    const hsnr = 'test';
    service['_setLatLong'](hsnr);

    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureAddress({ payload: failureHousenumber.uuid }));
  });

  it('should set the required values false if the brnach id is the right', () => {
    service.noBranchId = '123';
    service.currentFormState = { controls: { branchId: { value: '123' } as any } as any } as any;
    (service as any)._setDynamicRequired();
    expect(service.isFieldRequiredDependingOnBranchId).toBeFalsy();
  });

  it('should set the required values true if the brnach id is the not right', () => {
    service.noBranchId = '456';
    service.currentFormState = { controls: { branchId: { value: '123' } as any } as any } as any;
    (service as any)._setDynamicRequired();
    expect(service.isFieldRequiredDependingOnBranchId).toBeTruthy();
  });

  it('should set the noBranchId value if found OS Brnach', () => {
    const noBranch: FailureBranch = new FailureBranch();
    noBranch.id = '123';
    noBranch.name = 'OS';
    noBranch.description = 'ohne Sparte';
    const gasBranch: FailureBranch = new FailureBranch();
    gasBranch.id = '456';
    gasBranch.name = 'G';
    gasBranch.description = 'Gas';
    const branches: FailureBranch[] = [noBranch, gasBranch];
    service.gridFailureBranches$ = of(branches);
    service.noBranchId = '123';
    const spy1 = spyOn(service as any, '_setNoBranchId');
    const spy2 = spyOn(service as any, '_setGasBranchId');
    const spy3 = spyOn(service as any, '_setPowerBranchId');
    (service as any).setBranchIds();
    expect(service.noBranchId).toBe('123');
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(spy3).toHaveBeenCalled();
  });

  it('should not set the noBranchId value null if not found OS Brnach', () => {
    const branch: FailureBranch = new FailureBranch();
    branch.id = '123';
    branch.name = 'W';
    branch.description = 'Wasser';
    const branches: FailureBranch[] = [branch];
    service.gridFailureBranches$ = of(branches);
    service.noBranchId = '123';
    (service as any).setBranchIds();
    expect(service.noBranchId).toBe(null);
  });

  it('should setNoBranch id', () => {
    const branch: FailureBranch = new FailureBranch();
    branch.id = '123';
    branch.name = 'OS';
    branch.description = 'ohne Sparte';
    const branches: FailureBranch[] = [branch];
    service.gridFailureBranches$ = of(branches);
    (service as any).setBranchIds();
    expect(service.noBranchId).toBe('123');
  });

  it('should set gasBranch id', () => {
    const branch: FailureBranch = new FailureBranch();
    branch.id = '123';
    branch.name = 'G';
    branch.description = 'Gas';
    const branches: FailureBranch[] = [branch];
    service.gridFailureBranches$ = of(branches);
    (service as any).setBranchIds();
    expect(appState.dispatch).toHaveBeenCalledWith(new SetUserDefinedPropertyAction(FORM_ID, GAS_BRANCH_ID_KEY, '123'));
  });

  it('should call resetFailureLocationValues and dispatch an setValueAction', () => {
    const gfdetail = new GridFailure();
    gfdetail.id = 'id';
    gfdetail.responsibility = 'test';
    gfdetail.stationId = 'test';

    service.currentFormState = INITIAL_STATE;
    (service.currentFormState as any).isValid = true;
    (service.currentFormState as any).value = gfdetail;

    service.resetFailureLocationValues();
    expect(dispatchSpy).toHaveBeenCalled();
  });

  it('should call saveFailureLocationSpecificParts', () => {
    let part: string = 'NS';
    service.setViewStateForReqProps(part);
    expect(appState.dispatch).toHaveBeenCalledWith(new EnableAction(INITIAL_STATE.controls.city.id));
    expect(appState.dispatch).toHaveBeenCalledWith(new EnableAction(INITIAL_STATE.controls.radiusId.id));

    part = 'MS';
    service.setViewStateForReqProps(part);

    part = 'map';
    service.setViewStateForReqProps(part);
    expect(appState.dispatch).toHaveBeenCalledWith(new DisableAction(INITIAL_STATE.controls.postcode.id));
    expect(appState.dispatch).toHaveBeenCalledWith(new DisableAction(INITIAL_STATE.controls.radiusId.id));
  });

  it('should not call call saveFailureLocationSpecificParts if part is empty', () => {
    let part: string = '';
    service.setViewStateForReqProps(part);
    expect(appState.dispatch).not.toHaveBeenCalled();
  });

  it('should delete gridFailure station', () => {
    let station = new FailureStation();
    station.id = 'xx';
    station.latitude = 5;
    station.longitude = 5;
    service.gridFailureStations = [station];

    service.deleteGridFailureStation('xx');

    expect(service.gridFailureStations).toEqual([]);
  });

  it('should load gridFailure stations', () => {
    service.loadGridFailureStations('xx');
    service.loadGridFailureStations(null);

    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureStations({ payload: 'xx' }));
  });

  it('should set GridFailureId', () => {
    service.setGridFailureId('xx');

    expect((service as any)._gridFailureId).toBe('xx');
  });

  it('should set selected Station', () => {
    const station = new FailureStation();
    service.setSelectedStation(station);

    expect((service as any)._selectedStation).toBe(station);
  });

  it('should show alert if no station on post selected Station', () => {
    (service as any)._stations = [];
    const spy = spyOn(utilService, 'displayNotification');
    service.postSelectedStation();
    expect(spy).toHaveBeenCalled();
  });

  it('should do nothing if stations but not selectedstation on post selected Station', () => {
    const station = new FailureStation();
    station.id = 'xx';
    station.latitude = 1;
    station.longitude = 1;
    (service as any)._stations = [station];
    (service as any)._selectedStation = undefined;
    const spy = spyOn(utilService, 'displayNotification');
    service.postSelectedStation();
    expect(spy).not.toHaveBeenCalled();
    expect(dispatchSpy).not.toHaveBeenCalled();
  });

  it('should show message if selected station already assigned on post selected Station', () => {
    const station = new FailureStation();
    station.id = 'xx';
    station.latitude = 1;
    station.longitude = 1;
    (service as any)._stations = [station];
    (service as any)._selectedStation = station;
    service.gridFailureStations = [station];
    const spy = spyOn(utilService, 'displayNotification');
    service.postSelectedStation();
    expect(spy).toHaveBeenCalled();
    expect(dispatchSpy).not.toHaveBeenCalled();
  });

  it('should calculate polygon on post selected Station', () => {
    const station = new FailureStation();
    station.id = 'xx';
    station.latitude = 1;
    station.longitude = 1;
    (service as any)._stations = [station];
    (service as any)._selectedStation = station;
    service.gridFailureStations = [];
    const spy1 = spyOn(utilService, 'displayNotification');
    const spy2 = spyOn(service, 'latLonMapping');
    const spy3 = spyOn(service as any, '_calculatePolygon');
    service.postSelectedStation();
    expect(spy1).not.toHaveBeenCalledTimes(4);
    expect(dispatchSpy).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalled();
    expect(spy3).toHaveBeenCalled();
    expect((service as any)._selectedStation).toBeNull();
  });

  it('should delete station', () => {
    const gfdetail = new GridFailure();
    gfdetail.id = 'id';
    gfdetail.responsibility = 'test';
    (service.currentFormState as any).isValid = true;
    (service.currentFormState as any).value = gfdetail;
    const stationId = 'xx';
    (service as any)._gridFailureDetailId = 'x';
    service.deleteGridFailureStation(stationId);
    expect(appState.dispatch).toHaveBeenCalled();
  });

  it('should dispatch loadGridFailureDistributionGroups Action via loadGridFailureDistributionGroups(id)', () => {
    service.loadGridFailureDistributionGroups('id');
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailureDistributionGroups({ payload: 'id' }));
  });

  it('should dispatch loadDistributionGroups Action via loadDistributionGroups()', () => {
    service.loadDistributionGroups();
    expect(appState.dispatch).toHaveBeenCalledWith(distributionGroupActions.loadDistributionGroups());
  });

  it('should dispatch loadGridFailurePublicationChannels Action via loadGridFailurePublicationChannels()', () => {
    service.loadGridFailurePublicationChannels('x');
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadGridFailurePublicationChannels({ payload: 'x' }));
  });

  it('should compare maxVersionNumber vs. current versionNumber via checkForMaxVersion()', () => {
    service.maxVersionNumber = 1;
    service.currentFormState = { value: { versionNumber: 1 } as any } as any;
    expect(service.checkForMaxVersion()).toBeTruthy;
  });

  it('should dispatch deleteDistributionGroupAssignment Action via deleteDistributionGroupAssignment()', () => {
    service['_gridFailureId'] = 'x1';
    service.deleteDistributionGroupAssignment('x2');
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.deleteDistributionGroupAssignment({ gridFailureId: 'x1', groupId: 'x2' }));
  });

  it('should displayNotification when group is already assigned', () => {
    const spy: any = spyOn(service['_utilService'], 'displayNotification');
    service['_assignedDistributionGroups'] = [new DistributionGroup({ id: 'x' })];
    let selectedDistributionGroup = new DistributionGroup({ id: 'x' });
    service.createDistributionGroupAssignment(selectedDistributionGroup, false);
    expect(spy).toHaveBeenCalled;
  });

  it('should dispatch appropriate action when group is not already assigned', () => {
    service['_gridFailureId'] = 'x1';
    service['_assignedDistributionGroups'] = [new DistributionGroup({ id: 'x' })];
    let selectedDistributionGroup = new DistributionGroup({ id: 'y' });
    service.createDistributionGroupAssignment(selectedDistributionGroup, false);
    expect(appState.dispatch).toHaveBeenCalledWith(
      gridFailureActions.createDistributionGroupAssignment({ gridFailureId: 'x1', newGroup: selectedDistributionGroup })
    );
  });

  it('should displayNotification when publishing is attempted without at least one selected channel', () => {
    const spy: any = spyOn(service['_utilService'], 'displayNotification');
    service.selectedPublicationChannels = [];
    service.setPublishedState();
    expect(spy).toHaveBeenCalled;
  });

  it('should call saveGridFailure() when publishing with at least one selected channel', () => {
    const spy: any = spyOn(service, 'saveGridFailure');
    service.selectedPublicationChannels = ['MAIL'];
    service.setPublishedState();
    expect(spy).toHaveBeenCalled;
  });

  it('should dispatch createPublicationChannelAssignment action if a channel was checked', () => {
    service['_gridFailureId'] = 'x1';
    service.setPublicationChannels('SMS', true);
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.createPublicationChannelAssignment({ gridFailureId: 'x1', channel: 'SMS' }));
  });

  it('should dispatch deletePublicationChannelAssignment action if a channel was unchecked', () => {
    service['_gridFailureId'] = 'x1';
    service.setPublicationChannels('SMS', false);
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.deletePublicationChannelAssignment({ gridFailureId: 'x1', channel: 'SMS' }));
  });

  it('should dispatch action in response to controlId = gridFailureDetailsForm.postcode', () => {
    let formState: any = {
      id: 'gridFailureDetailsForm',
      value: {
        statusIntern: StateEnum.CREATED,
        postcode: null,
        branch: null,
      },
      userDefinedProperties: { controlId: 'gridFailureDetailsForm.postcode' },
      controls: {
        postcode: { id: 'gridFailureDetailsForm.postcode' },
        __formBranch: { id: 'test' },
      },
    };
    (service as any)._handleUserDefinedProperties(formState);
    const communities = gridFailureActions.loadAddressStreets({
      community: null,
      postcode: formState.value.postcode,
      branch: formState.value.branch,
      district: null,
    });
    expect(appState.dispatch).toHaveBeenCalledWith(communities);
  });

  it('should dispatch action in response to controlId = gridFailureDetailsForm.city', () => {
    let formState: any = {
      id: '',
      value: {
        statusIntern: StateEnum.CREATED,
        branch: null,
        city: 'S',
      },
      userDefinedProperties: { controlId: 'gridFailureDetailsForm.city' },
      controls: {
        postcode: { id: 'gridFailureDetailsForm.postcode' },
        __formBranch: { id: 'test' },
        city: { id: 'gridFailureDetailsForm.city' },
      },
    };
    (service as any)._handleUserDefinedProperties(formState);
    expect(appState.dispatch).toHaveBeenCalledWith(
      gridFailureActions.loadAddressDistrictsOfCommunity({
        branch: formState.value.branch,
        community: formState.value.city,
      })
    );
  });

  it('should dispatch action in response to controlId = gridFailureDetailsForm.street', () => {
    let formState: any = {
      id: 'gridFailureDetailsForm',
      value: {
        statusIntern: StateEnum.CREATED,
        branch: 'G',
        postcode: '00000',
        city: 'X',
        district: 'Y',
        street: 'Z',
      },
      userDefinedProperties: { controlId: 'gridFailureDetailsForm.street' },
      controls: {
        __formBranch: { id: 'test' },
        postcode: { id: 'gridFailureDetailsForm.postcode' },
        city: { id: 'gridFailureDetailsForm.city' },
        district: { id: 'gridFailureDetailsForm.district' },
        street: { id: 'gridFailureDetailsForm.street' },
      },
    };
    (service as any)._handleUserDefinedProperties(formState);
    expect(appState.dispatch).toHaveBeenCalledWith(
      gridFailureActions.loadAddressHouseNumbers({
        branch: formState.value.branch,
        postcode: formState.value.postcode,
        community: formState.value.city,
        street: formState.value.street,
      })
    );
  });

  it('should dispatch action in response to controlId = gridFailureDetailsForm.housenumber', () => {
    const spy: any = spyOn<any>(service, '_setLatLong');
    let formState: any = {
      id: 'gridFailureDetailsForm',
      value: {
        statusIntern: StateEnum.CREATED,
        branch: 'G',
        postcode: '00000',
        city: 'X',
        district: 'Y',
        street: 'Z',
        housenumber: '1a',
      },
      userDefinedProperties: { controlId: 'gridFailureDetailsForm.housenumber' },
      controls: {
        __formBranch: { id: 'test' },
        postcode: { id: 'gridFailureDetailsForm.postcode' },
        city: { id: 'gridFailureDetailsForm.city' },
        district: { id: 'gridFailureDetailsForm.district' },
        street: { id: 'gridFailureDetailsForm.street' },
        housenumber: { id: 'gridFailureDetailsForm.housenumber' },
      },
    };
    (service as any)._handleUserDefinedProperties(formState);
    expect(spy).toHaveBeenCalledWith(formState.controls.housenumber.value);
  });

  it('should dispatch action in response to controlId = gridFailureDetailsForm', () => {
    const spy: any = spyOn<any>(service, '_loadInitialAddressData');
    let formState: any = {
      id: 'gridFailureDetailsForm',
      value: {
        statusIntern: StateEnum.CREATED,
        branch: 'G',
        postcode: '00000',
        city: 'X',
        district: 'Y',
        street: 'Z',
        housenumber: '1a',
      },
      userDefinedProperties: { controlId: 'gridFailureDetailsForm' },
      controls: {
        __formBranch: { id: 'test' },
        postcode: { id: 'gridFailureDetailsForm.postcode' },
        city: { id: 'gridFailureDetailsForm.city' },
        district: { id: 'gridFailureDetailsForm.district' },
        street: { id: 'gridFailureDetailsForm.street' },
        housenumber: { id: 'gridFailureDetailsForm.housenumber' },
      },
    };
    (service as any)._handleUserDefinedProperties(formState);
    expect(spy).toHaveBeenCalledWith(formState);
  });

  it('should set the form state as dirty', () => {
    service.setFormStateDirty();
    expect(dispatchSpy).toHaveBeenCalledTimes(1);
  });

  it('should set the form state as pristine', () => {
    service.setFormStatePristine();
    expect(dispatchSpy).toHaveBeenCalledTimes(1);
  });

  it('should call createDistributionGroupAssignment if autoset is true', () => {
    const group = { id: 1, autoSet: true };
    const spy = spyOn(service, 'getValidDistributionGroups').and.returnValue([group]);
    const spyCreateDistributionGroupAssignment = spyOn(service, 'createDistributionGroupAssignment');
    (service as any).setDistributionGroupAssignments();
    expect(spyCreateDistributionGroupAssignment).toHaveBeenCalledWith(group as any, false);
    expect(spy).toHaveBeenCalled();
  });

  it('should setBranchIds - return if gfiId is null', () => {
    (service as any)._gridFailureId = null;
    const spySetBranchIds = spyOn(service, 'setBranchIds');

    service.branchClassificationChanged();

    expect(spySetBranchIds).toHaveBeenCalled();
  });

  it('should handle assignments if branch clasifiaction changed and group not found', () => {
    (service as any)._gridFailureId = 'test_id';
    const spySetBranchIds = spyOn(service, 'setBranchIds');
    const groups = [new DistributionGroup({ id: '1' }), new DistributionGroup({ id: '2' })];
    service.gridFailureDistributionGroups$ = of(groups);

    const validgroups = [new DistributionGroup({ id: '3', autoSet: true }), new DistributionGroup({ id: '4', autoSet: true })];
    spyOn(service, 'getValidDistributionGroups').and.returnValue(validgroups);
    const spyDelete = spyOn(service, 'deleteDistributionGroupAssignment');
    const spyCreate = spyOn(service, 'createDistributionGroupAssignment');

    service.branchClassificationChanged();

    expect(spySetBranchIds).toHaveBeenCalled();
    expect(spyDelete).toHaveBeenCalledTimes(2);
    expect(spyCreate).toHaveBeenCalledTimes(2);
  });

  it('should handle assignments if branch clasifiaction changed and group found', () => {
    (service as any)._gridFailureId = 'test_id';
    const spySetBranchIds = spyOn(service, 'setBranchIds');
    const groups = [new DistributionGroup({ id: '1' }), new DistributionGroup({ id: '2' })];
    service.gridFailureDistributionGroups$ = of(groups);

    const validgroups = [new DistributionGroup({ id: '2', autoSet: true }), new DistributionGroup({ id: '4', autoSet: true })];
    spyOn(service, 'getValidDistributionGroups').and.returnValue(validgroups);
    const spyDelete = spyOn(service, 'deleteDistributionGroupAssignment');
    const spyCreate = spyOn(service, 'createDistributionGroupAssignment');

    service.branchClassificationChanged();

    expect(spySetBranchIds).toHaveBeenCalled();
    expect(spyDelete).toHaveBeenCalledTimes(1);
    expect(spyCreate).toHaveBeenCalledTimes(2);
  });

  it('should getValidDistributionGroups - key exists', () => {
    service.currentFormState.value.branchId = 'branchId';
    service.currentFormState.value.failureClassificationId = 'failureClassificationId';
    service.distributionGroupsState.key = 'branchIdfailureClassificationId1';
    const groups = [new DistributionGroup()];
    service.distributionGroupsState.distributionGroups = groups;
    service.distributionGroups = groups;

    const res = service.getValidDistributionGroups();

    expect(res).toBe(groups);
  });

  it('should getValidDistributionGroups - key doesnt exist', () => {
    service.currentFormState.value.branchId = 'branchId';
    service.currentFormState.value.failureClassificationId = 'failureClassificationId';
    service.distributionGroupsState.key = undefined;
    const groups = [new DistributionGroup({ allowedGroups: [], autoSet: true })];
    service.distributionGroupsState.distributionGroups = groups;
    service.distributionGroups = groups;

    const res = service.getValidDistributionGroups();

    expect(service.distributionGroupsState.key).toBe('branchIdfailureClassificationId1');
  });

  it('should getValidDistributionGroups - key doesnt exist - allowedGroups exists - dif ids', () => {
    service.currentFormState.value.branchId = 'branchId';
    service.currentFormState.value.failureClassificationId = 'failureClassificationId';
    service.distributionGroupsState.key = undefined;
    const groups = [new DistributionGroup({ allowedGroups: [{ branchId: 'branchId', classificationId: 'classificationId' }], autoSet: true })];
    service.distributionGroupsState.distributionGroups = groups;
    service.distributionGroups = groups;

    const res = service.getValidDistributionGroups();

    expect(res).toEqual([]);
  });

  it('should getValidDistributionGroups - key doesnt exist - allowedGroups exists - same ids', () => {
    service.currentFormState.value.branchId = 'branchId';
    service.currentFormState.value.failureClassificationId = 'classificationId';
    service.distributionGroupsState.key = undefined;
    const groups = [new DistributionGroup({ allowedGroups: [{ branchId: 'branchId', classificationId: 'classificationId', autoSet: true }], autoSet: true })];
    service.distributionGroupsState.distributionGroups = groups;
    service.distributionGroups = groups;

    const res = service.getValidDistributionGroups();

    expect(service.distributionGroupsState.key).toBe('branchIdclassificationId1');
  });

  it('should dispatch loadGridFailureCommunities Action via loadAllAddressCommunities()', () => {
    service.loadGridFailureCommunities();
    expect(appState.dispatch).toHaveBeenCalledWith(gridFailureActions.loadAllAddressCommunities({ branch: null }));
  });

  it('should dispatch setFaultLocationArea Action via SetValueAction()', () => {
    const area = 'arena';
    service.setFaultLocationArea(area);
    expect(appState.dispatch).toHaveBeenCalled();
  });

  it('should dispatch setBranchState Action via SetValueAction()', () => {
    const branch = new FailureBranch();
    service.setBranchState(branch);
    expect(appState.dispatch).toHaveBeenCalled();
  });

  it('should lookupAddressForPosition - return if is not map', () => {
    const nominatimUrl = 'url';
    const longitude = 123;
    const latitude = 456;
    (service as any).isMap = false;
    const spySetAddress = spyOn(service as any, '_setAddress');

    service.lookupAddressForPosition(nominatimUrl, longitude, latitude);

    expect(spySetAddress).not.toHaveBeenCalled();
  });

  it('should lookupAddressForPosition - call setAddress if is map', () => {
    const nominatimUrl = 'url';
    const longitude = 123;
    const latitude = 456;
    (service as any).isMap = true;
    service.currentFormState.value.freetextCity = 'freetextCity';
    service.currentFormState.value.freetextPostcode = 'freetextPostcode';
    service.currentFormState.value.freetextDistrict = 'freetextDistrict';

    const spySetAddress = spyOn(service as any, '_setAddress');
    spyOn(service['_modalService'], 'open').and.returnValue({ componentInstance: { title: '', body: '' }, result: { then: () => of(false) } } as any);

    const data = { anydata: 'anydata' };
    const spyNomdata = spyOn(externalApiClient, 'getNominatimData').and.returnValue(of(data));

    service.lookupAddressForPosition(nominatimUrl, longitude, latitude);

    expect(spySetAddress).not.toHaveBeenCalled();
    expect(spyNomdata).toHaveBeenCalled();
  });

  it('should lookupAddressForPosition - call setAddress if is map and no freetext is defined', () => {
    const nominatimUrl = 'url';
    const longitude = 123;
    const latitude = 456;
    (service as any).isMap = true;
    service.currentFormState.value.freetextCity = undefined;
    service.currentFormState.value.freetextPostcode = undefined;
    service.currentFormState.value.freetextDistrict = undefined;

    const spySetAddress = spyOn(service as any, '_setAddress');

    const data = { anydata: 'anydata' };
    const spyNomdata = spyOn(externalApiClient, 'getNominatimData').and.returnValue(of(data));

    service.lookupAddressForPosition(nominatimUrl, longitude, latitude);

    expect(spySetAddress).toHaveBeenCalledWith(data);
    expect(spyNomdata).toHaveBeenCalled();
  });

  it('should _setAddress', () => {
    const city = 'my_city';
    const district = 'my_district';
    const spyCity = spyOn(service as any, '_getCityFromNominatim').and.returnValue(city);
    const spyDistrict = spyOn(service as any, '_getDistrictFromNominatim').and.returnValue(district);

    const data = { address: { postcode: 12345 } };

    (service as any)._setAddress(data);

    expect(appState.dispatch).toHaveBeenCalled();
    expect(spyCity).toHaveBeenCalled();
    expect(spyDistrict).toHaveBeenCalled();
  });

  it('should _getDistrictFromNominatim - if suburb', () => {
    const data = { address: { suburb: 'suburb', locality: 'locality', municipality: 'municipality', leisure: 'leisure', county: 'county' } };
    const res = (service as any)._getDistrictFromNominatim(data);
    expect(res).toBe('suburb');
  });

  it('should _getDistrictFromNominatim - if locality', () => {
    const data = { address: { locality: 'locality', municipality: 'municipality', leisure: 'leisure', county: 'county' } };
    const res = (service as any)._getDistrictFromNominatim(data);
    expect(res).toBe('locality');
  });

  it('should _getDistrictFromNominatim - if municipality', () => {
    const data = { address: { municipality: 'municipality', leisure: 'leisure', county: 'county' } };
    const res = (service as any)._getDistrictFromNominatim(data);
    expect(res).toBe('municipality');
  });

  it('should _getDistrictFromNominatim - if leisure', () => {
    const data = { address: { leisure: 'leisure', county: 'county' } };
    const res = (service as any)._getDistrictFromNominatim(data);
    expect(res).toBe('leisure');
  });

  it('should _getDistrictFromNominatim - if county', () => {
    const data = { address: { county: 'county' } };
    const res = (service as any)._getDistrictFromNominatim(data);
    expect(res).toBe('county');
  });

  it('should _getCityFromNominatim - if city', () => {
    const data = { address: { city: 'city', town: 'town', village: 'village', state: 'state' } };
    const res = (service as any)._getCityFromNominatim(data);
    expect(res).toBe('city');
  });

  it('should _getCityFromNominatim - if town', () => {
    const data = { address: { town: 'town', village: 'village', state: 'state' } };
    const res = (service as any)._getCityFromNominatim(data);
    expect(res).toBe('town');
  });

  it('should _getCityFromNominatim - if village', () => {
    const data = { address: { village: 'village', state: 'state' } };
    const res = (service as any)._getCityFromNominatim(data);
    expect(res).toBe('village');
  });

  it('should _getCityFromNominatim - if state', () => {
    const data = { address: { state: 'state' } };
    const res = (service as any)._getCityFromNominatim(data);
    expect(res).toBe('state');
  });

  it('should showForDistStatus - true for status NEW', () => {
    service.currentFormState.value.statusIntern = StateEnum.NEW;
    const res = service.showForDistStatus();
    expect(res).toBeTruthy();
  });

  it('should showForDistStatus - false for status COMPLETED', () => {
    service.currentFormState.value.statusIntern = StateEnum.COMPLETED;
    const res = service.showForDistStatus();
    expect(res).toBeFalsy();
  });

  it('should showForDistStatus - false for status CANCELED', () => {
    service.currentFormState.value.statusIntern = StateEnum.CANCELED;
    const res = service.showForDistStatus();
    expect(res).toBeFalsy();
  });

  it('should dispatch loadPreConfiguration Action via loadPreConfiguration()', () => {
    service.loadPreConfiguration();
    expect(appState.dispatch).toHaveBeenCalledWith(preConfigurationActions.loadPreConfiguration());
  });

  it('should _loadInitialAddressData - undefined formState', () => {
    const formState = {} as any;
    (service as any)._loadInitialAddressData(formState);
    expect(appState.dispatch).not.toHaveBeenCalled();
  });

  it('should _loadInitialAddressData - defined formState', () => {
    const formState = { controls: {}, value: { branch: 'branch', city: 'city', district: 'district', postcode: 'postcode', street: 'street' } } as any;
    (service as any)._loadInitialAddressData(formState);
    expect(appState.dispatch).toHaveBeenCalledTimes(5);
  });
});
