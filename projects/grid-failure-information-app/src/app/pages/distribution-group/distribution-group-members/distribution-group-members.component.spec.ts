/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { DistributionGroupMembersComponent } from '@grid-failure-information-app/pages/distribution-group/distribution-group-members/distribution-group-members.component';
import { DistributionGroupMember } from '@grid-failure-information-app/shared/models';
import { of } from 'rxjs';
import { RowClickedEvent, ModelUpdatedEvent, RowNode, GridApi } from 'ag-grid-community';

describe('DistributionGroupMembersComponent', () => {
  let component: DistributionGroupMembersComponent;
  let sandbox: DistributionGroupSandbox;
  let actionsSubject = { pipe: () => of({}) } as any;
  let contactModelChangedSubject = { pipe: () => of({}), next: () => of({}) } as any;

  beforeEach(() => {
    sandbox = {
      registerEvents() {},
      endSubscriptions() {},
      deleteDistributionGroupMember() {},
      transformPostcodes() {},
      setSelectedDistributionGroupMember() {},
      actionsSubject: actionsSubject,
      contactModelChangedSubject$: contactModelChangedSubject,
    } as any;
    component = new DistributionGroupMembersComponent(sandbox);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call appropriate function for delete event', () => {
    const spy: any = spyOn(sandbox, 'deleteDistributionGroupMember');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'delete', data: new DistributionGroupMember() });
    expect(spy).toHaveBeenCalled();
  });

  it('should not call delete function for other (not delete) event', () => {
    const spy: any = spyOn(sandbox, 'deleteDistributionGroupMember');
    component.ngOnInit();
    component.gridOptions.context.eventSubject.next({ type: 'test', data: new DistributionGroupMember() });
    expect(spy).not.toHaveBeenCalled();
  });

  it('should clear search input', () => {
    component.searchInput = { nativeElement: { value: 'x' } };
    component.clearSearchInput();
    expect(component.searchInput.nativeElement.value).toBeFalsy();
  });

  it('should unsubscribe OnDestroy', () => {
    const spy: any = spyOn(component['_endSubscriptions$'], 'next' as any);
    component.ngOnDestroy();
    expect(spy).toHaveBeenCalled();
  });

  it('should call getDisplayedRowAtIndex on onModelUpdate event in ngOnInit()', () => {
    let event = {
      api: {
        getDisplayedRowAtIndex(i: number) {
          return {
            setSelected(j: boolean): void {
            },
          } as RowNode;
        },
      } as unknown as GridApi,
    } as ModelUpdatedEvent;
    const spy: any = spyOn(event.api, 'getDisplayedRowAtIndex');
    component.ngOnInit();
    component.gridOptions.onModelUpdated(event);
    expect(spy).toHaveBeenCalled();
  });

  it('should set selectedMemberRowIndex on cellClickedEvent', () => {
    let event: RowClickedEvent = {
      type: 'cellClicked',
      data: new DistributionGroupMember(),
      rowIndex: 0,
      column: {
        getColDef() {
          return { field: 'x' };
        },
      },
    } as any;
    component.ngOnInit();
    component.gridOptions.onRowClicked(event);
    expect(sandbox.selectedMemberRowIndex).toBe(0);
  });

  it('should set initial sorting', () => {
    let sortModel;
    let params = { api: { setFilterModel: sortModel = {} } as any };
    component['_gridApi'] = params.api;
    const spy: any = spyOn(component['_gridApi'], 'setFilterModel' as any);
    component.onGridReady(params);
    expect(spy).toHaveBeenCalled();
  });
});
