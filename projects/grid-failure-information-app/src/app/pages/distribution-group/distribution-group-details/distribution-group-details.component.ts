/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { KeyValue } from '@angular/common';
import { Component } from '@angular/core';
import { DistributionGroupSandbox } from '@grid-failure-information-app/app/pages/distribution-group/distribution-group.sandbox';
import { DistributionPublicationStatusEnum, DistributionsGroupTextTypeEnum } from '@grid-failure-information-app/shared/constants/enums';

@Component({
  selector: 'app-distribution-group-details',
  templateUrl: './distribution-group-details.component.html',
  styleUrls: ['./distribution-group-details.component.scss'],
})
export class DistributionGroupDetailsComponent {
  public DistributionPublicationStatusEnum = DistributionPublicationStatusEnum;
  public DistributionsGroupTextTypeEnum = DistributionsGroupTextTypeEnum;

  constructor(public sandbox: DistributionGroupSandbox) {}

  valueComperator = (a: KeyValue<string, string>, b: KeyValue<string, string>): number => {
    return a.value.localeCompare(b.value);
  };

  public changeTemplate(selectedPublicationStatusForTemplate: DistributionPublicationStatusEnum, selectedTextTypeForTemplate: DistributionsGroupTextTypeEnum) {
    this.sandbox.selectedPublicationStatusForTemplate = selectedPublicationStatusForTemplate;
    this.sandbox.selectedTextTypeForTemplate = selectedTextTypeForTemplate;

    this.sandbox.changeEmailTemplate(
      selectedPublicationStatusForTemplate,
      this.sandbox.oldPublicationStatusForTemplate,
      selectedTextTypeForTemplate,
      this.sandbox.oldTextTypeForTemplate
    );
    this.sandbox.oldPublicationStatusForTemplate = selectedPublicationStatusForTemplate;
    this.sandbox.oldTextTypeForTemplate = selectedTextTypeForTemplate;
  }
}
