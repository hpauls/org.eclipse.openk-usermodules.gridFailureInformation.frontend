/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import {
  HttpService,
  GET,
  Adapter,
  DefaultHeaders,
  Path,
  PUT,
  POST,
  Body,
  DELETE,
  Query,
  Headers,
} from '@grid-failure-information-app/shared/async-services/http';
import { Observable, of } from 'rxjs';
import { DistributionGroupService } from '@grid-failure-information-app/pages/distribution-group/distribution-group.service';
import { DistributionGroup, DistributionGroupMember, Contact, DistributionGroupTextPlaceholder } from '@grid-failure-information-app/shared/models';

@Injectable()
@DefaultHeaders({
  Accept: 'application/json',
  'Content-Type': 'application/json',
})
export class DistributionGroupApiClient extends HttpService {
  @GET('/distribution-groups')
  @Adapter(DistributionGroupService.gridAdapter)
  public getDistributionGroups(): Observable<DistributionGroup[]> {
    return null;
  }

  @GET('/distribution-groups/{id}')
  @Adapter(DistributionGroupService.itemAdapter)
  public getDistributionGroupDetails(@Path('id') groupId: string): Observable<DistributionGroup> {
    return null;
  }

  @PUT('/distribution-groups/{id}')
  @Adapter(DistributionGroupService.itemAdapter)
  public putDistributionGroup(@Path('id') id: string, @Body() gridFailure: DistributionGroup): Observable<DistributionGroup> {
    return null;
  }

  @POST('/distribution-groups')
  @Adapter(DistributionGroupService.itemAdapter)
  public postDistributionGroup(@Body() newDistributionGroup: DistributionGroup): Observable<DistributionGroup> {
    return null;
  }

  @DELETE('/distribution-groups/{id}')
  @Adapter(DistributionGroupService.itemAdapter)
  public deleteDistributionGroup(@Path('id') id: string): Observable<void> {
    return null;
  }

  @GET('/distribution-groups/{id}/members')
  @Adapter(DistributionGroupService.memberGridAdapter)
  public getDistributionGroupMembers(@Path('id') id: string): Observable<DistributionGroupMember[]> {
    return null;
  }

  @DELETE('/distribution-groups/{groupId}/members/{memberId}')
  @Adapter(DistributionGroupService.itemAdapter)
  public deleteDistributionGroupMember(@Path('groupId') groupId: string, @Path('memberId') memberId: string): Observable<void> {
    return null;
  }

  @POST('/distribution-groups/{id}/members')
  @Adapter(DistributionGroupService.itemAdapter)
  public postDistributionGroupMember(@Path('id') id: string, @Body() newDistributionGroupMember: DistributionGroupMember): Observable<DistributionGroupMember> {
    return null;
  }

  @PUT('/distribution-groups/{groupId}/members/{memberId}')
  @Adapter(DistributionGroupService.itemAdapter)
  public putDistributionGroupMember(
    @Path('groupId') groupId: string,
    @Path('memberId') memberId: string,
    @Body() distributionGroupMember: DistributionGroupMember
  ): Observable<DistributionGroupMember> {
    return null;
  }

  @GET('/contacts')
  @Adapter(DistributionGroupService.contactsPageAdapter)
  public getContacts(@Query('searchText') searchtext: string): Observable<Contact[]> {
    return null;
  }

  @GET('/placeholders')
  @Adapter(DistributionGroupService.placeholderAdapter)
  public getDistributionGroupTextPlaceholders(): Observable<DistributionGroupTextPlaceholder> {
    return null;
  }

  @Headers({
    Accept: '*',
    'Content-Type': 'text/csv',
  })
  @GET('/distribution-groups/{id}/members/csv')
  public exportContacts(@Path('id') id: string): Observable<any> {
    return null;
  }
}
