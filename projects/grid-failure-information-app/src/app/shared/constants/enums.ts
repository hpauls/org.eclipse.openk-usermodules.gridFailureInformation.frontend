/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export enum DatePickerResetEnum {
  FailureBegin = 'failureBegin',
  FailureEndPlanned = 'failureEndPlanned',
  FailureEndResupplied = 'failureEndResupplied',
}
export enum VoltageLevelEnum {
  NS = 'Niederspannung',
  MS = 'Mittelspannung',
  HS = 'Hochspannung',
}
export enum PressureLevelEnum {
  ND = 'Niederdruck',
  MD = 'Mitteldruck',
  HD = 'Hochdruck',
}
export enum PublicationStatusEnum {
  NV = 'nicht veröffentlicht',
  VE = 'veröffentlicht',
}

export enum DistributionPublicationStatusEnum {
  PUBLISH = 'publish',
  COMPLETE = 'complete',
  UPDATE = 'update',
}

export enum RolesEnum {
  ADMIN = 'grid-failure-admin',
  CREATOR = 'grid-failure-creator',
  PUBLISHER = 'grid-failure-publisher',
  QUALIFIER = 'grid-failure-qualifier',
  READER = 'grid-failure-reader',
}

export enum StateEnum {
  NEW = 'neu',
  PLANNED = 'geplant',
  CREATED = 'angelegt',
  CANCELED = 'storniert',
  QUALIFIED = 'qualifiziert',
  UPDATED = 'aktualisiert',
  COMPLETED = 'abgeschlossen',
  PUBLISHED = 'veröffentlicht',
  DELETED = 'gelöscht',
  WITHDRAWN = 'zurückgezogen',
}
export enum ModeEnum {
  InitialMode = 'initialMode',
  OverviewTableSelectionMode = 'overviewTableSelectionMode',
  CondensationTableSelectionMode = 'condensationTableSelectionMode',
  CondensationTableNoSelectionMode = 'condensationTableNoSelectionMode',
  oldVersionMode = 'oldVersionMode',
  currentVersionMode = 'currentVersionMode',
}

export enum EventTypeEnum {
  Edit = 'edit',
  Readonly = 'readonly',
  Add = 'add',
  AddAllItems = 'addAllItems',
  InitialLoad = 'initialLoad',
  Remove = 'remove',
  LoadCondensedItems = 'loadCondensedItems',
}

export enum BranchNameEnum {
  NO_BRANCH = 'OS',
  GAS = 'G',
  POWER = 'S',
  WATER = 'W',
  TELECOMMUNICATIONS = 'TK',
  DISTRICT_HEATING = 'F',
  SECONDARY_TECHNIQUE = 'ST',
}

export enum VisibilityEnum {
  SHOW = 'show',
  HIDE = 'hide',
}

export enum FaultLocationAreaEnum {
  Address = 'address',
  Station = 'station',
  Map = 'map',
}

export enum DistributionsGroupTextTypeEnum {
  Short = 'short',
  Long = 'long',
}
