 /********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';

/** Provides all Version infomrations
 *
 *
 * @author Martin Gardyan <martin.gardyan@pta.de>
 * @export
 * @class VersionInfoSandbox
 */
@Injectable()
export class VersionInfoSandbox {
  /**
   * Creates an instance of VersionInfoSandbox.
   *
   * @author Martin Gardyan <martin.gardyan@pta.de>
   * @memberof VersionInfoSandbox
   */
  constructor() {}
}
