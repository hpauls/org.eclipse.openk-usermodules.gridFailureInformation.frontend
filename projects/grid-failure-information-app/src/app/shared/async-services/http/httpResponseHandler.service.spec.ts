import { ActivatedRoute } from '@angular/router';
/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { HttpResponseHandler } from '@grid-failure-information-app/shared/async-services/http/httpResponseHandler.service';

describe('HttpResponseHandler with undefined configService->unauthorizedEndpoints', () => {
  let component: HttpResponseHandler;
  let router: any;
  let translateService: any;
  let notificationsService: any;
  let configService: any;
  let activatedRoute: any;

  beforeEach(() => {
    router = {
      navigate() {},
    } as any;
    translateService = {
      instant() {},
    };
    notificationsService = {
      error() {},
      warn() {},
      info() {},
    };
    configService = {
      get: () => ({ options: 'options', unauthorizedEndpoints: [] }),
    };

    component = new HttpResponseHandler(router, translateService, notificationsService, configService, activatedRoute);
  });

  it('should create the component', () => {
    expect(component).toBeTruthy();
  });

  it('should call handleBadRequest when response status is 400', () => {
    const spy = spyOn(component as any, 'handleBadRequest');
    const response = { status: 400 };
    component.onCatch(response, {} as any);
    expect(spy).toHaveBeenCalled();
  });

  it('should call handleUnauthorized when response status is 401', () => {
    const spy = spyOn(component as any, 'handleUnauthorized');
    const response = { status: 401 };
    component.onCatch(response, {} as any);
    expect(spy).toHaveBeenCalled();
  });

  it('should call handleForbidden when response status is 403', () => {
    const spy = spyOn(component as any, 'handleForbidden');
    const response = { status: 403 };
    component.onCatch(response, {} as any);
    expect(spy).toHaveBeenCalled();
  });

  it('should call handleNotFound when response status is 404', () => {
    const spy = spyOn(component as any, 'handleNotFound');
    const response = { status: 404 };
    component.onCatch(response, {} as any);
    expect(spy).toHaveBeenCalled();
  });

  it('should call handleRefExists when response status is 409', () => {
    const spy = spyOn(component as any, 'handleRefExists');
    const response = { status: 409 };
    component.onCatch(response, {} as any);
    expect(spy).toHaveBeenCalled();
  });

  it('should call handleServerError when response status is 500', () => {
    const spy = spyOn(component as any, 'handleServerError');
    const response = { status: 500 };
    component.onCatch(response, {} as any);
    expect(spy).toHaveBeenCalled();
  });

  it('should break when response status is not one from the defined', () => {
    const spy = spyOn(component as any, 'handleBadRequest');
    const spy2 = spyOn(component as any, 'handleUnauthorized');
    const spy3 = spyOn(component as any, 'handleForbidden');
    const spy4 = spyOn(component as any, 'handleNotFound');
    const spy5 = spyOn(component as any, 'handleRefExists');
    const spy6 = spyOn(component as any, 'handleServerError');
    const response = { status: 999 };
    component.onCatch(response, {} as any);
    expect(spy).not.toHaveBeenCalled();
    expect(spy2).not.toHaveBeenCalled();
    expect(spy3).not.toHaveBeenCalled();
    expect(spy4).not.toHaveBeenCalled();
    expect(spy5).not.toHaveBeenCalled();
    expect(spy6).not.toHaveBeenCalled();
  });

  it('should handle an error message when calling handleBadRequest with a valid body', () => {
    const spy = spyOn(component as any, 'handleErrorMessages');
    const responseBody: any = {
      _body: {
        timestamp: '2020-03-26T09:05:01.564+0000',
        status: 400,
      },
      json() {},
    };
    (component as any).handleBadRequest(responseBody);
    expect(spy).toHaveBeenCalled();
  });

  it('should handle a server error when calling handleBadRequest with a not valid body', () => {
    const spy = spyOn(component as any, 'handleServerError');
    const responseBody: any = {
      _body: null,
      json() {},
    };
    (component as any).handleBadRequest(responseBody);
    expect(spy).toHaveBeenCalled();
  });

  it('should not show a 401 notification when calling handleUnauthorized without unauthorizedEndpoints', () => {
    const spy = spyOn(translateService, 'instant');
    const spy1 = spyOn(notificationsService, 'info');
    const spy2 = spyOn(router, 'navigate');

    component['activatedRoute'] = { snapshot: { queryParams: 'x' } as any } as any;

    const responseBody: any = {
      url: 'endpoint1' as string,
    };
    (component as any).handleUnauthorized(responseBody);
    expect(spy).not.toHaveBeenCalledWith('ServerError401');
    expect(spy1).not.toHaveBeenCalled();
    expect(spy2).toHaveBeenCalledWith(['/loggedout']);
  });

  it('should show a 403 notification and navigate to login when calling handleForbidden', () => {
    const spy = spyOn(translateService, 'instant');
    const spy2 = spyOn(notificationsService, 'error');
    const spy3 = spyOn(router, 'navigate');

    (component as any).handleForbidden();
    expect(spy).toHaveBeenCalledWith('ServerError403');
    expect(spy2).toHaveBeenCalled();
    expect(spy3).toHaveBeenCalledWith(['/login']);
  });

  it('should show an 404 error notification when calling handleNotFound', () => {
    const spy = spyOn(translateService, 'instant');
    const spy2 = spyOn(component as any, 'showNotificationError');

    (component as any).handleNotFound();
    expect(spy).toHaveBeenCalledWith('ServerError404');
    expect(spy2).toHaveBeenCalled();
  });

  it('should show an warning notification when calling handleRefExists', () => {
    const spy = spyOn(translateService, 'instant');
    const spy2 = spyOn(component as any, 'showNotificationWarning');

    (component as any).handleRefExists();
    expect(spy).toHaveBeenCalledWith('ServerError409');
    expect(spy2).toHaveBeenCalled();
  });

  it('should show an 500 error notification when calling handleServerError', () => {
    const spy = spyOn(translateService, 'instant');
    const spy2 = spyOn(component as any, 'showNotificationError');

    (component as any).handleServerError();
    expect(spy).toHaveBeenCalledWith('ServerError500');
    expect(spy2).toHaveBeenCalled();
  });

  it('should return without action calling handleErrorMessages with undefined response', () => {
    const spy = spyOn(component as any, 'showNotificationError');
    const spy2 = spyOn(component as any, 'getTranslatedValue');

    const response: any = undefined;

    (component as any).handleErrorMessages(response);
    expect(spy).not.toHaveBeenCalled();
    expect(spy2).not.toHaveBeenCalled();
  });

  it('should show one error notification when calling handleErrorMessages and there is only one error', () => {
    const spy = spyOn(component as any, 'showNotificationError');
    const response: any = {
      key: '[value1]',
    };

    (component as any).handleErrorMessages(response);
    expect(spy).toHaveBeenCalled();
  });

  it('should show as many error notification as responses when calling handleErrorMessages and there are more than one errors', () => {
    const spy = spyOn(component as any, 'showNotificationError');
    const spy2 = spyOn(component as any, 'getTranslatedValue');

    const response: any = { key: ['[value1]', '[value2]', '[value3]'] };

    (component as any).handleErrorMessages(response);
    expect(spy).toHaveBeenCalledTimes(3);
    expect(spy2).toHaveBeenCalled();
  });

  it('should get the translated value when calling getTranslatedValue', () => {
    const value: string = 'value';

    const result = (component as any).getTranslatedValue(value);
    expect(result).toBe(value);
  });

  it('should call notifications service with an error message when calling showNotificationError', () => {
    const title: string = 'title';
    const message: string = 'message';
    const spy = spyOn(notificationsService, 'error');

    (component as any).showNotificationError(title, message);
    expect(spy).toHaveBeenCalled();
  });
});

describe('HttpResponseHandler with defined configService->unauthorizedEndpoints', () => {
  let component: HttpResponseHandler;
  let router: any;
  let translateService: any;
  let notificationsService: any;
  let configService: any;
  let activatedRoute: any;

  beforeEach(() => {
    router = {
      navigate() {},
    } as any;
    translateService = {
      instant() {},
    };
    notificationsService = {
      error() {},
      info() {},
    };
    configService = {
      get: () => ({ options: 'options', unauthorizedEndpoints: ['endpoint1', 'endpoint2'] }),
    };

    component = new HttpResponseHandler(router, translateService, notificationsService, configService, activatedRoute);
  });

  it('should show a 401 notification when calling handleUnauthorized with unauthorizedEndpoints', () => {
    const spy = spyOn(translateService, 'instant');
    const spy1 = spyOn(notificationsService, 'info');
    const spy2 = spyOn(router, 'navigate');

    component['activatedRoute'] = { snapshot: { queryParams: 'x' } as any } as any;

    const responseBody: any = {
      url: 'endpoint1' as string,
    };
    (component as any).handleUnauthorized(responseBody);
    expect(spy).toHaveBeenCalledWith('ServerError401');
    expect(spy1).toHaveBeenCalled();
    expect(spy2).toHaveBeenCalledWith(['/loggedout']);
  });
});
