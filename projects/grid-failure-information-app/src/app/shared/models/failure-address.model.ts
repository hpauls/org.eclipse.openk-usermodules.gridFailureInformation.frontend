/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class FailureAddress {
  public id: string = null;
  public sdox1: number = null;
  public sdoy1: number = null;
  public g3efid: number = null;
  public postcode: string = null;
  public community: string = null;
  public district: string = null;
  public street: string = null;
  public housenumber: string = null;
  public waterConnection: boolean = null;
  public waterGroup: string = null;
  public gasConnection: boolean = null;
  public gasGroup: string = null;
  public powerConnection: boolean = null;
  public stationId: string = null;
  public longitude: number = null;
  public latitude: number = null;

  public constructor(data: any = null) {
    Object.keys(data || {})
      .filter(property => this.hasOwnProperty(property))
      .forEach(property => (this[property] = data[property]));
  }
}
