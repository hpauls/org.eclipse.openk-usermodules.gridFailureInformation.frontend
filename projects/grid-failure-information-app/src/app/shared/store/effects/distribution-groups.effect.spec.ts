/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { take } from 'rxjs/operators';
import { DistributionGroupsEffects } from '@grid-failure-information-app/shared/store/effects/distribution-groups.effect';
import { Subject, of, throwError } from 'rxjs';
import { DistributionGroup, DistributionGroupMember, Contact, DistributionGroupTextPlaceholder } from '@grid-failure-information-app/shared/models';
import * as distributionGroupActions from '@grid-failure-information-app/shared/store/actions/distribution-groups.action';
import { DistributionGroupApiClient } from '@grid-failure-information-app/pages/distribution-group/distribution-group-api-client';
import { Store } from '@ngrx/store';

describe('DistributionGroups Effects', () => {
  let effects: DistributionGroupsEffects;
  let actions$: Subject<any>;
  let apiClient: DistributionGroupApiClient;
  let store: Store<any>;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getDistributionGroups() {},
      getDistributionGroupDetails() {},
      putDistributionGroup() {},
      postDistributionGroup() {},
      deleteDistributionGroup() {},
      getDistributionGroupMembers() {},
      deleteDistributionGroupMember() {},
      getContacts() {},
      postDistributionGroupMember() {},
      getDistributionGroupTextPlaceholders() {},
      exportContacts() {},
      putDistributionGroupMember() {},
    } as any;
    store = {
      dispatch() {},
    } as any;
    actions$ = new Subject();
    effects = new DistributionGroupsEffects(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadDistributionGroupsSuccess after getDistributionGroups', done => {
    apiResponse = [new DistributionGroup({ id: '1' })];
    spyOn(apiClient, 'getDistributionGroups').and.returnValue(of(apiResponse));
    effects.getDistributionGroups$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadDistributionGroupsSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.loadDistributionGroups());
  });

  it('should equal loadDistributionGroupsFail after getDistributionGroups Error', done => {
    spyOn(apiClient, 'getDistributionGroups').and.returnValue(throwError('error'));
    effects.getDistributionGroups$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadDistributionGroupsFail({ payload: 'error' }));
    });
    done();
    actions$.next(distributionGroupActions.loadDistributionGroups());
  });

  it('should equal loadDistributionGroupsDetailSuccess after getDistributionGroupDetails', done => {
    apiResponse = new DistributionGroup({ id: '1' });
    spyOn(apiClient, 'getDistributionGroupDetails').and.returnValue(of(apiResponse));
    effects.getDistributionGroupsDetail$.pipe(take(1)).subscribe(result => {
      const actionResult = distributionGroupActions.loadDistributionGroupsDetailSuccess({ payload: apiResponse });
      expect(result.payload.id).toEqual(actionResult.payload.id);
    });
    done();
    actions$.next(distributionGroupActions.loadDistributionGroupsDetail({ payload: '1' }));
  });

  it('should equal loadDistributionGroupsDetailFail after getDistributionGroupDetails Error', done => {
    spyOn(apiClient, 'getDistributionGroupDetails').and.returnValue(throwError('error'));
    effects.getDistributionGroupsDetail$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadDistributionGroupsDetailFail({ payload: 'error' }));
    });
    done();
    actions$.next(distributionGroupActions.loadDistributionGroupsDetail({ payload: '1' }));
  });

  it('should equal saveDistributionGroupSuccess after saveDistributionGroup (put)', done => {
    apiResponse = new DistributionGroup({ id: '1' });
    spyOn(apiClient, 'putDistributionGroup').and.returnValue(of(apiResponse));
    effects.saveDistributionGroup$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.saveDistributionGroupSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.saveDistributionGroup({ payload: new DistributionGroup({ id: '1' }) }));
  });

  it('should equal saveDistributionGroupSuccess after saveDistributionGroup (post)', done => {
    apiResponse = new DistributionGroup();
    spyOn(apiClient, 'postDistributionGroup').and.returnValue(of(apiResponse));
    effects.saveDistributionGroup$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.saveDistributionGroupSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.saveDistributionGroup({ payload: new DistributionGroup() }));
  });

  it('should equal saveDistributionGroupFail after saveDistributionGroup Error', done => {
    spyOn(apiClient, 'postDistributionGroup').and.returnValue(throwError('x'));
    effects.saveDistributionGroup$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.saveDistributionGroupFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.saveDistributionGroup({ payload: new DistributionGroup() }));
  });

  it('should equal deleteDistributionGroupSuccess after deleteDistributionGroup', done => {
    spyOn(apiClient, 'deleteDistributionGroup').and.returnValue(of(null));
    effects.deleteDistributionGroup$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.deleteDistributionGroupSuccess());
    });
    done();
    actions$.next(distributionGroupActions.deleteDistributionGroup({ payload: '1' }));
  });

  it('should equal deleteDistributionGroupFail after deleteDistributionGroup Error', done => {
    spyOn(apiClient, 'deleteDistributionGroup').and.returnValue(throwError('x'));
    effects.deleteDistributionGroup$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.deleteDistributionGroupFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.deleteDistributionGroup({ payload: '1' }));
  });

  it('should equal loadDistributionGroupMembersSuccess after getDistributionGroupMembers', done => {
    apiResponse = [new DistributionGroupMember({ id: '1' })];
    spyOn(apiClient, 'getDistributionGroupMembers').and.returnValue(of(apiResponse));
    effects.getDistributionGroupMembers$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadDistributionGroupMembersSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.loadDistributionGroupMembers({ payload: '1' }));
  });

  it('should equal loadDistributionGroupMembersFail after getDistributionGroupMembers Error', done => {
    spyOn(apiClient, 'getDistributionGroupMembers').and.returnValue(throwError('x'));
    effects.getDistributionGroupMembers$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadDistributionGroupMembersFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.loadDistributionGroupMembers({ payload: '1' }));
  });

  it('should equal deleteDistributionGroupMemberSuccess after deleteDistributionGroupMember$', done => {
    spyOn(apiClient, 'deleteDistributionGroupMember').and.returnValue(of(null));
    effects.deleteDistributionGroupMember$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.deleteDistributionGroupMemberSuccess());
    });
    done();
    actions$.next(distributionGroupActions.deleteDistributionGroupMember({ groupId: 'x', memberId: 'y' }));
  });

  it('should equal deleteDistributionGroupMemberFail after deleteDistributionGroupMember$ Error', done => {
    spyOn(apiClient, 'deleteDistributionGroupMember').and.returnValue(throwError('x'));
    effects.deleteDistributionGroupMember$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.deleteDistributionGroupMemberFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.deleteDistributionGroupMember({ groupId: 'x', memberId: 'y' }));
  });

  it('should equal loadContactsSuccess after getContacts', done => {
    apiResponse = [new Contact({ id: '1' })];
    spyOn(apiClient, 'getContacts').and.returnValue(of(apiResponse));
    effects.getContacts$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadContactsSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.loadContacts({ searchText: 'x' }));
  });

  it('should equal loadContactsFail after getContacts Error', done => {
    spyOn(apiClient, 'getContacts').and.returnValue(throwError('x'));
    effects.getContacts$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadContactsFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.loadContacts({ searchText: 'x' }));
  });

  it('should equal createDistributionGroupMemberSuccess after createDistributionGroupMember was called', done => {
    apiResponse = [new DistributionGroupMember({ id: '1' })];
    spyOn(apiClient, 'postDistributionGroupMember').and.returnValue(of(apiResponse));
    effects.createDistributionGroupMember$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.createDistributionGroupMemberSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.createDistributionGroupMember({ groupId: 'x', newMember: new DistributionGroupMember({ id: '1' }) }));
  });

  it('should equal createDistributionGroupMemberFail after createDistributionGroupMember Error', done => {
    spyOn(apiClient, 'postDistributionGroupMember').and.returnValue(throwError('x'));
    effects.createDistributionGroupMember$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.createDistributionGroupMemberFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.createDistributionGroupMember({ groupId: 'x', newMember: new DistributionGroupMember({ id: '1' }) }));
  });

  it('should equal updateDistributionGroupMemberSuccess after putDistributionGroupMember was called', done => {
    apiResponse = [new DistributionGroupMember({ id: '1' })];
    spyOn(apiClient, 'putDistributionGroupMember').and.returnValue(of(apiResponse));
    effects.putDistributionGroupMember$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.updateDistributionGroupMemberSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.updateDistributionGroupMember({ groupId: 'x', memberId: 'y', member: new DistributionGroupMember({ id: '1' }) }));
  });

  it('should equal updateDistributionGroupMemberFail after putDistributionGroupMember Error', done => {
    spyOn(apiClient, 'putDistributionGroupMember').and.returnValue(throwError('x'));
    effects.putDistributionGroupMember$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.updateDistributionGroupMemberFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.updateDistributionGroupMember({ groupId: 'x', memberId: 'y', member: new DistributionGroupMember({ id: '1' }) }));
  });

  it('should equal loadDistributionGroupTextPlaceholdersSuccess after getDistributionTextPlaceholders was called', done => {
    apiResponse = new DistributionGroupTextPlaceholder();
    spyOn(apiClient, 'getDistributionGroupTextPlaceholders').and.returnValue(of(apiResponse));
    effects.getDistributionTextPlaceholders$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadDistributionGroupTextPlaceholdersSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.loadDistributionGroupTextPlaceholders());
  });

  it('should equal loadDistributionGroupTextPlaceholdersFail after getDistributionTextPlaceholders Error', done => {
    spyOn(apiClient, 'getDistributionGroupTextPlaceholders').and.returnValue(throwError('x'));
    effects.getDistributionTextPlaceholders$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.loadDistributionGroupTextPlaceholdersFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.loadDistributionGroupTextPlaceholders());
  });

  it('should equal exportContactsSuccess after exportContacts was called', done => {
    apiResponse = new Object();
    spyOn(apiClient, 'exportContacts').and.returnValue(of(apiResponse));
    effects.exportContacts$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.exportContactsSuccess({ payload: apiResponse }));
    });
    done();
    actions$.next(distributionGroupActions.exportContacts({ groupId: 'x' }));
  });

  it('should equal exportContactsFail after exportContacts throws an error', done => {
    spyOn(apiClient, 'exportContacts').and.returnValue(throwError('x'));
    effects.exportContacts$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(distributionGroupActions.exportContactsFail({ payload: 'x' }));
    });
    done();
    actions$.next(distributionGroupActions.exportContacts({ groupId: 'x' }));
  });
});
