/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { take } from 'rxjs/operators';
import { GridFailuresEffects } from '@grid-failure-information-app/shared/store/effects/grid-failures.effect';
import { Subject, of, throwError } from 'rxjs';
import {
  GridFailure,
  FailureBranch,
  FailureClassification,
  FailureState,
  FailureRadius,
  FailureExpectedReason,
  FailureStation,
  FailureAddress,
  DistributionGroup,
  PublicationChannel,
} from '@grid-failure-information-app/shared/models';
import * as gridFailureActions from '@grid-failure-information-app/shared/store/actions/grid-failures.action';
import { GridFailureApiClient } from '@grid-failure-information-app/pages/grid-failure/grid-failure-api-client';
import { Store } from '@ngrx/store';

describe('GridFailure Effects', () => {
  let effects: GridFailuresEffects;
  let actions$: Subject<any>;
  let apiClient: GridFailureApiClient;
  let store: Store<any>;
  let apiResponse: any = null;

  beforeEach(() => {
    apiClient = {
      getGridFailures() {},
      getGridFailureDetails() {},
      putGridFailure() {},
      postGridFailure() {},
      getGridFailureVersions() {},
      getGridFailureVersion() {},
      getGridFailureBranches() {},
      getGridFailureClassifications() {},
      getGridFailureStates() {},
      getGridFailureRadii() {},
      getGridFailureExpectedReasons() {},
      postGridFailuresCondensation() {},
      putGridFailuresCondensation() {},
      getCondensedGridFailures() {},
      getStations() {},
      getAddressPostalcodes() {},
      getAllAddressCommunities() {},
      getAddressDistrictsOfCommunity() {},
      getAddressStreets() {},
      getAddressHousenumbers() {},
      getAddress() {},
      getGridFailureDistributionGroups() {},
      postDistributionGroupAssignment() {},
      deleteDistributionGroupAssignment() {},
      postPublicationChannelAssignment() {},
      deletePublicationChannelAssignment() {},
      getGridFailureStations() {},
      postGridFailureStation() {},
      deleteGridFailureStation() {},
      deleteGridFailure() {},
      getGridFailurePolygon() {},
      getFailureReminder() {},
      getGridFailurePublicationChannels() {},
    } as any;
    store = {
      dispatch() {},
    } as any;
    actions$ = new Subject();
    effects = new GridFailuresEffects(actions$, apiClient, store);
  });

  it('should be truthy', () => {
    expect(effects).toBeTruthy();
  });

  it('should equal loadGridFailuresSuccess after getGridFailures', () => {
    apiResponse = [new GridFailure({ id: '1' })];
    spyOn(apiClient, 'getGridFailures').and.returnValue(of(apiResponse));
    effects.getGridFailures$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailuresSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailures());
  });

  it('should equal loadGridFailuresFail after getAddressType Error', () => {
    spyOn(apiClient, 'getGridFailures').and.returnValue(throwError('x'));
    effects.getGridFailures$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailuresFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailures());
  });

  it('should equal loadGridFailureDetailSuccess in response to getGridFailureDetails', () => {
    apiResponse = new GridFailure({ id: '1' });
    spyOn(apiClient, 'getGridFailureDetails').and.returnValue(of(apiResponse));
    effects.getGridFailureDetails$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureDetailSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureDetail({ payload: '1' }));
  });

  it('should equal loadGridFailureDetailSuccess in response to getGridFailureDetails Error', () => {
    spyOn(apiClient, 'getGridFailureDetails').and.returnValue(throwError('x'));
    effects.getGridFailureDetails$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureDetailFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureDetail({ payload: '1' }));
  });

  it('should equal saveGridFailureSuccess in response to saveGridFailure', () => {
    apiResponse = new GridFailure({ id: '1' });
    spyOn(apiClient, 'putGridFailure').and.returnValue(of(apiResponse));
    effects.saveGridFailure$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.saveGridFailureSuccess({ payload: new GridFailure({ id: '1' }) }));
    });

    actions$.next(gridFailureActions.saveGridFailure({ payload: { gridFailure: new GridFailure({ id: '1' }), saveForPublish: false } }));
  });

  it('should equal saveGridFailureFail in response to saveGridFailure Error', () => {
    spyOn(apiClient, 'putGridFailure').and.returnValue(throwError('x'));
    effects.saveGridFailure$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.saveGridFailureFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.saveGridFailure({ payload: { gridFailure: new GridFailure({ id: '1' }), saveForPublish: false } }));
  });

  it('should equal loadGridFailureVersionsSuccess after getGridFailureVersions', () => {
    apiResponse = [new GridFailure({ id: '1' })];
    spyOn(apiClient, 'getGridFailureVersions').and.returnValue(of(apiResponse));
    effects.getGridFailureVersions$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureVersionsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureVersions({ payload: '1' }));
  });

  it('should equal loadGridFailureVersionsFail after getGridFailureVersions Error', () => {
    spyOn(apiClient, 'getGridFailureVersions').and.returnValue(throwError('x'));
    effects.getGridFailureVersions$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureVersionsFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureVersions({ payload: '1' }));
  });

  it('should equal loadGridFailureVersion in response to getGridFailureVersion', () => {
    apiResponse = new GridFailure({ id: '1' });
    spyOn(apiClient, 'getGridFailureVersion').and.returnValue(of(apiResponse));
    effects.getGridFailureVersion$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureVersionSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureVersion({ gridFailureId: '1', versionNumber: 1 }));
  });

  it('should equal loadGridFailureVersionFail in response to getGridFailureVersion Error', () => {
    spyOn(apiClient, 'getGridFailureVersion').and.returnValue(throwError('x'));
    effects.getGridFailureVersion$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureVersionFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureVersion({ gridFailureId: '1', versionNumber: 1 }));
  });

  it('should equal loadGridFailureBranchesSuccess after getGridFailureBranches', () => {
    apiResponse = [new FailureBranch({ id: '1' })];
    spyOn(apiClient, 'getGridFailureBranches').and.returnValue(of(apiResponse));
    effects.getGridFailureBranches$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureBranchesSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureBranches());
  });

  it('should equal loadGridFailureBranchesFail in response to getGridFailureBranches Error', () => {
    spyOn(apiClient, 'getGridFailureBranches').and.returnValue(throwError('x'));
    effects.getGridFailureBranches$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureBranchesFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureBranches());
  });

  it('should equal loadGridFailureClassifications after getGridFailureClassifications', () => {
    apiResponse = [new FailureClassification({ id: '1' })];
    spyOn(apiClient, 'getGridFailureClassifications').and.returnValue(of(apiResponse));
    effects.getGridFailureClassifications$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureClassificationsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureClassifications());
  });

  it('should equal loadGridFailureClassificationsFail in response to getGridFailureClassifications Error', () => {
    spyOn(apiClient, 'getGridFailureClassifications').and.returnValue(throwError('x'));
    effects.getGridFailureClassifications$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureClassificationsFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureClassifications());
  });

  it('should equal loadGridFailureStates after getGridFailureStates', () => {
    apiResponse = [new FailureState({ id: '1' })];
    spyOn(apiClient, 'getGridFailureStates').and.returnValue(of(apiResponse));
    effects.getGridFailureStates$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureStatesSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureStates());
  });

  it('should equal loadGridFailureStatesFail in response to getGridFailureStates Error', () => {
    spyOn(apiClient, 'getGridFailureStates').and.returnValue(throwError('x'));
    effects.getGridFailureStates$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureStatesFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureStates());
  });

  it('should equal loadGridFailureRadii after getGridFailureRadii', () => {
    apiResponse = [new FailureRadius({ id: '1' })];
    spyOn(apiClient, 'getGridFailureRadii').and.returnValue(of(apiResponse));
    effects.getGridFailureRadii$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureRadiiSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureRadii());
  });

  it('should equal loadGridFailureRadiiFail in response to getGridFailureRadii Error', () => {
    spyOn(apiClient, 'getGridFailureRadii').and.returnValue(throwError('x'));
    effects.getGridFailureRadii$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureRadiiFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureRadii());
  });

  it('should equal loadGridFailureExpectedReasons after getGridFailureExpectedReasons', () => {
    apiResponse = [new FailureExpectedReason({ id: '1' })];
    spyOn(apiClient, 'getGridFailureExpectedReasons').and.returnValue(of(apiResponse));
    effects.getGridFailureExpectedReasons$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureExpectedReasonsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureExpectedReasons(undefined));
  });

  it('should equal loadGridFailureExpectedReasonsFail in response to getGridFailureExpectedReasons Error', () => {
    spyOn(apiClient, 'getGridFailureExpectedReasons').and.returnValue(throwError('x'));
    effects.getGridFailureExpectedReasons$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureExpectedReasonsFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureExpectedReasons(undefined));
  });

  it('should equal postGridFailuresCondensationSuccess in response to postCondensedGridFailures', () => {
    apiResponse = [new GridFailure({ id: '1' })];
    spyOn(apiClient, 'postGridFailuresCondensation').and.returnValue(of(apiResponse));
    effects.postGridFailuresCondensation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.postGridFailuresCondensationSuccess());
    });

    actions$.next(gridFailureActions.postGridFailuresCondensation({ payload: apiResponse }));
  });

  it('should equal postGridFailuresCondensationFail in response to postGridFailuresCondensation Error', () => {
    apiResponse = [new GridFailure({ id: '1' })];
    spyOn(apiClient, 'postGridFailuresCondensation').and.returnValue(throwError('x'));
    effects.postGridFailuresCondensation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.postGridFailuresCondensationFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.postGridFailuresCondensation({ payload: apiResponse }));
  });

  it('should equal deleteGridFailureSuccess after deleteGridFailure', () => {
    spyOn(apiClient, 'deleteGridFailure').and.returnValue(of(null));
    effects.deleteGridFailure$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.deleteGridFailureSuccess());
    });
    actions$.next(gridFailureActions.deleteGridFailure({gridFailureId:'123'}));
  });

  it('should equal deleteGridFailureFail after deleteGridFailure Error', () => {
    spyOn(apiClient, 'deleteGridFailure').and.returnValue(throwError('errorXXX'));
    effects.deleteGridFailure$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.deleteGridFailureFail({ error: 'errorXXX' }));
    });

    actions$.next(gridFailureActions.deleteGridFailure({ gridFailureId: '123' }));
  });



  it('should equal getCondensedGridFailuresSuccess after getCondensedGridFailures', () => {
    apiResponse = [new GridFailure({ id: '1' })];
    spyOn(apiClient, 'getCondensedGridFailures').and.returnValue(of(apiResponse));
    effects.getCondensedGridFailures$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadCondensedGridFailuresSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadCondensedGridFailures({ payload: apiResponse }));
  });

  it('should equal getCondensedGridFailuresFail after getCondensedGridFailures Error', () => {
    spyOn(apiClient, 'getCondensedGridFailures').and.returnValue(throwError('x'));
    effects.getCondensedGridFailures$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadCondensedGridFailuresFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadCondensedGridFailures({ payload: apiResponse }));
  });

  it('should equal putGridFailuresCondensationSuccess in response to putCondensedGridFailures', () => {
    apiResponse = [new GridFailure({ id: '1' })];
    const id = 'test';
    spyOn(apiClient, 'putGridFailuresCondensation').and.returnValue(of(apiResponse));
    effects.putGridFailuresCondensation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.putGridFailuresCondensationSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.putGridFailuresCondensation({ gridFailureId: id, payload: apiResponse }));
  });

  it('should equal putGridFailuresCondensationFail in response to putGridFailuresCondensation Error', () => {
    const id = 'test';
    spyOn(apiClient, 'putGridFailuresCondensation').and.returnValue(throwError('x'));
    effects.putGridFailuresCondensation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.putGridFailuresCondensationFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.putGridFailuresCondensation({ gridFailureId: id, payload: apiResponse }));
  });

  it('should equal getStationsSuccess after getStations', () => {
    apiResponse = [new FailureStation({ id: '1' })];
    spyOn(apiClient, 'getStations').and.returnValue(of(apiResponse));
    effects.getStations$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadStationsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadStations());
  });

  it('should equal getStationsFail in response to getStations Error', () => {
    spyOn(apiClient, 'getStations').and.returnValue(throwError('x'));
    effects.getStations$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadStationsFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadStations());
  });

  it('should return [] in getAddressPostalcodes if community and distict is null', () => {
    apiResponse = [];
    spyOn(apiClient, 'getAddressPostalcodes').and.returnValue(of(apiResponse));
    effects.getAddressPostalcodes$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressPostalcodesSuccess({ payload: apiResponse }));
    });
    actions$.next(gridFailureActions.loadAddressPostalcodes({ branch: null, community: null, district:null }));
  });

  it('should return spy value in getAddressPostalcodes if community and district have values', () => {
    apiResponse = ['33333'];
    spyOn(apiClient, 'getAddressPostalcodes').and.returnValue(of(apiResponse));
    effects.getAddressPostalcodes$.pipe(take(1)).subscribe(result => {
      const postalcodes = gridFailureActions.loadAddressPostalcodesSuccess({ payload: apiResponse });
      expect(result).toEqual(postalcodes);
    });
    actions$.next(gridFailureActions.loadAddressPostalcodes({ branch: null, community: 'df', district:'df' }));
  });

  it('should equal loadAddressPostalcodes Fail after getAddressPostalcodes Error', () => {
    spyOn(apiClient, 'getAddressPostalcodes').and.returnValue(throwError('x'));
    effects.getAddressPostalcodes$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressPostalcodesFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadAddressPostalcodes({ branch: null, community: 'df', district:'df' }));
  });

  it('should return spy value in getAllAddressCommunities if branch is null', () => {
    apiResponse = ['X'];
    spyOn(apiClient, 'getAllAddressCommunities').and.returnValue(of(apiResponse));
    effects.getAllAddressCommunities$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAllAddressCommunitiesSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadAllAddressCommunities({ branch: null }));
  });

  it('should equal loadAllAddressCommunities Fail after getAllAddressCommunities Error', () => {
    spyOn(apiClient, 'getAllAddressCommunities').and.returnValue(throwError('x'));
    effects.getAllAddressCommunities$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAllAddressCommunitiesFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadAllAddressCommunities({ branch: null }));
  });

  it('should return spy value in getAddressDistricts if community is set ', () => {
    apiResponse = ['D'];
    spyOn(apiClient, 'getAddressDistrictsOfCommunity').and.returnValue(of(apiResponse));
    effects.getAddressDistricts$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressDistrictsOfCommunitySuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadAddressDistrictsOfCommunity({ branch: null, community: 'C' }));
  });

  it('should return empty Array via getAddressDistricts if postcode is empty', () => {
    apiResponse = [];
    spyOn(apiClient, 'getAddressDistrictsOfCommunity').and.returnValue(of(apiResponse));
    effects.getAddressDistricts$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressDistrictsOfCommunitySuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadAddressDistrictsOfCommunity({  branch: null, community: null }));
  });

  it('should equal loadAddressDistricts Fail after getAddressDistricts Error', () => {
    spyOn(apiClient, 'getAddressDistrictsOfCommunity').and.returnValue(throwError('x'));
    effects.getAddressDistricts$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressDistrictsOfCommunityFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadAddressDistrictsOfCommunity({branch: null, community: 'C' }));
  });

  it('should equal loadAddressStreets after getAddressStreets', () => {
    apiResponse = ['S'];
    spyOn(apiClient, 'getAddressStreets').and.returnValue(of(apiResponse));
    effects.getAddressStreets$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressStreetsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadAddressStreets({ postcode: '33333', branch: null, community: 'C', district: 'D' }));
  });

  it('should return empty Array via getAddressStreets if postcode is empty', () => {
    apiResponse = [];
    spyOn(apiClient, 'getAddressStreets').and.returnValue(of(apiResponse));
    effects.getAddressStreets$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressStreetsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadAddressStreets({ postcode: '', branch: null, community: 'C', district: 'D' }));
  });

  it('should equal loadAddressStreets Fail after getAddressStreets Error', () => {
    spyOn(apiClient, 'getAddressStreets').and.returnValue(throwError('x'));
    effects.getAddressStreets$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressStreetsFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadAddressStreets({ postcode: '33333', branch: null, community: 'C', district: 'D' }));
  });

  it('should equal loadAddressHouseNumbers after getAddressHousenumbers', () => {
    apiResponse = ['1a'];
    spyOn(apiClient, 'getAddressHousenumbers').and.returnValue(of(apiResponse));
    effects.getAddressHousenumbers$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressHouseNumbersSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadAddressHouseNumbers({ postcode: '33333', branch: null, community: 'C', street: 'S' }));
  });

  it('should return empty Array via getAddressHousenumbers if postcode is empty', () => {
    apiResponse = [];
    spyOn(apiClient, 'getAddressHousenumbers').and.returnValue(of(apiResponse));
    effects.getAddressHousenumbers$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressHouseNumbersSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadAddressHouseNumbers({ postcode: '', branch: null, community: 'C', street: 'S' }));
  });

  it('should equal loadAddressHouseNumbers Fail after getAddressHousenumbers Error', () => {
    spyOn(apiClient, 'getAddressHousenumbers').and.returnValue(throwError('x'));
    effects.getAddressHousenumbers$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadAddressHouseNumbersFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadAddressHouseNumbers({ postcode: '33333', branch: null, community: 'C', street: 'S' }));
  });

  it('should equal loadGridFailureVersion in response to getGridFailureAddress', () => {
    apiResponse = new FailureAddress({ id: '1' });
    spyOn(apiClient, 'getAddress').and.returnValue(of(apiResponse));
    effects.getGridFailureAddress$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureAddressSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureAddress({ payload: '1' }));
  });

  it('should equal loadGridFailureVersion Fail after getGridFailureAddress Error', () => {
    spyOn(apiClient, 'getAddress').and.returnValue(throwError('x'));
    effects.getGridFailureAddress$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureAddressFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureAddress({ payload: '1' }));
  });

  it('should equal loadGridFailureStations in response to getGridFailureStations', () => {
    apiResponse = new FailureStation({ id: '1' });
    spyOn(apiClient, 'getGridFailureStations').and.returnValue(of(apiResponse));
    effects.getGridFailureStations$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureStationsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureStations({ payload: '1' }));
  });

  it('should equal loadGridFailureStations Fail after getGridFailureStations Error', () => {
    spyOn(apiClient, 'getGridFailureStations').and.returnValue(throwError('x'));
    effects.getGridFailureStations$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureStationsFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureStations({ payload: '1' }));
  });

  it('should equal postGridFailureStationSuccess in response to postGridFailureStation', () => {
    apiResponse = new FailureStation({ id: '1' });
    const gridFailureId = 'x';
    spyOn(apiClient, 'postGridFailureStation').and.returnValue(of(apiResponse));
    effects.postGridFailureStation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.postGridFailureStationSuccess());
    });

    actions$.next(gridFailureActions.postGridFailureStation({ gridFailureDetailId: gridFailureId, station: apiResponse }));
  });

  it('should equal postGridFailureStationFail in response to postGridFailureStation Error', () => {
    apiResponse = new FailureStation({ id: '1' });
    const gridFailureId = 'x';
    spyOn(apiClient, 'postGridFailureStation').and.returnValue(throwError('x'));
    effects.postGridFailureStation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.postGridFailureStationFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.postGridFailureStation({ gridFailureDetailId: gridFailureId, station: apiResponse }));
  });

  it('should equal deleteGridFailureStationSuccess after deleteGridFailureStation', () => {
    apiResponse = new FailureStation({ id: '1' });
    const gridFailureId = 'x';
    spyOn(apiClient, 'deleteGridFailureStation').and.returnValue(of(null));
    effects.deleteGridFailureStation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.deleteGridFailureStationSuccess());
    });

    actions$.next(gridFailureActions.deleteGridFailureStation({ gridFailureDetailId: gridFailureId, stationId: apiResponse }));
  });

  it('should equal deleteGridFailureStationFail after deleteGridFailureStation Error', () => {
    apiResponse = new FailureStation({ id: '1' });
    const gridFailureId = 'x';
    spyOn(apiClient, 'deleteGridFailureStation').and.returnValue(throwError('x'));
    effects.deleteGridFailureStation$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.deleteGridFailureStationFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.deleteGridFailureStation({ gridFailureDetailId: gridFailureId, stationId: apiResponse }));
  });

  it('should dispatch loadGridFailureDistributionGroupsSuccess action triggered by loadGridFailureDistributionGroups action', () => {
    apiResponse = [new DistributionGroup({ id: '1' })];
    spyOn(apiClient, 'getGridFailureDistributionGroups').and.returnValue(of(apiResponse));
    effects.getGridFailureDistributionGroups$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureDistributionGroupsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailureDistributionGroups({ payload: '1' }));
  });

  it('should dispatch loadGridFailureDistributionGroupsFail action triggered by loadGridFailureDistributionGroups Error', () => {
    spyOn(apiClient, 'getGridFailureDistributionGroups').and.returnValue(throwError('x'));
    effects.getGridFailureDistributionGroups$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailureDistributionGroupsFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailureDistributionGroups({ payload: '1' }));
  });

  it('should dispatch createDistributionGroupAssignmentSuccess action triggered by createDistributionGroupAssignment action', () => {
    apiResponse = new DistributionGroup({ id: '1' });
    spyOn(apiClient, 'postDistributionGroupAssignment').and.returnValue(of(apiResponse));
    effects.createDistributionGroupAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.createDistributionGroupAssignmentSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.createDistributionGroupAssignment({ gridFailureId: '1', newGroup: new DistributionGroup({ id: '1' }) }));
  });

  it('should dispatch createDistributionGroupAssignmentFail action triggered by createDistributionGroupAssignment Error', () => {
    spyOn(apiClient, 'postDistributionGroupAssignment').and.returnValue(throwError('x'));
    effects.createDistributionGroupAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.createDistributionGroupAssignmentFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.createDistributionGroupAssignment({ gridFailureId: '1', newGroup: new DistributionGroup({ id: '1' }) }));
  });

  it('should dispatch deleteDistributionGroupAssignmentSuccess action triggered by deleteDistributionGroupAssignment action', () => {
    spyOn(apiClient, 'deleteDistributionGroupAssignment').and.returnValue(of(null));
    effects.deleteDistributionGroupAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.deleteDistributionGroupAssignmentSuccess());
    });

    actions$.next(gridFailureActions.deleteDistributionGroupAssignment({ gridFailureId: '1', groupId: '2' }));
  });

  it('should dispatch deleteDistributionGroupAssignmentFail action triggered by deleteDistributionGroupAssignment Error', () => {
    spyOn(apiClient, 'deleteDistributionGroupAssignment').and.returnValue(throwError('x'));
    effects.deleteDistributionGroupAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.deleteDistributionGroupAssignmentFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.deleteDistributionGroupAssignment({ gridFailureId: '1', groupId: '2' }));
  });

  it('should dispatch loadGridFailurePublicationChannelsSuccess action triggered by loadGridFailurePublicationChannels action', () => {
    apiResponse = [new PublicationChannel({ id: '1' })];
    spyOn(apiClient, 'getGridFailurePublicationChannels').and.returnValue(of(apiResponse));
    effects.getGridFailurePublicationChannels$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailurePublicationChannelsSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailurePublicationChannels({ payload: '1' }));
  });

  it('should dispatch loadGridFailurePublicationChannelsFail action triggered by loadGridFailurePublicationChannels action Error', () => {
    spyOn(apiClient, 'getGridFailurePublicationChannels').and.returnValue(throwError('x'));
    effects.getGridFailurePublicationChannels$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailurePublicationChannelsFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailurePublicationChannels({ payload: '1' }));
  });

  it('should dispatch createPublicationChannelAssignmentSuccess action triggered by createPublicationChannelAssignment action', () => {
    apiResponse = new PublicationChannel({ failureInformationId: '1' });
    spyOn(apiClient, 'postPublicationChannelAssignment').and.returnValue(of(apiResponse));
    effects.createPublicationChannelAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.createPublicationChannelAssignmentSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.createPublicationChannelAssignment({ gridFailureId: '1', channel: 'SMS' }));
  });

  it('should dispatch createPublicationChannelAssignmentFail action triggered by createPublicationChannelAssignment Error', () => {
    spyOn(apiClient, 'postPublicationChannelAssignment').and.returnValue(throwError('x'));
    effects.createPublicationChannelAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.createPublicationChannelAssignmentFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.createPublicationChannelAssignment({ gridFailureId: '1', channel: 'SMS' }));
  });

  it('should dispatch deletePublicationChannelAssignmentSuccess action triggered by deletePublicationChannelAssignment action', () => {
    spyOn(apiClient, 'deletePublicationChannelAssignment').and.returnValue(of(null));
    effects.deletePublicationChannelAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.deletePublicationChannelAssignmentSuccess());
    });

    actions$.next(gridFailureActions.deletePublicationChannelAssignment({ gridFailureId: '1', channel: 'SMS' }));
  });

  it('should dispatch deletePublicationChannelAssignmentFail action triggered by deletePublicationChannelAssignment Error', () => {
    spyOn(apiClient, 'deletePublicationChannelAssignment').and.returnValue(throwError('x'));
    effects.deletePublicationChannelAssignment$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.deletePublicationChannelAssignmentFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.deletePublicationChannelAssignment({ gridFailureId: '1', channel: 'SMS' }));
  });

  it('should equal loadGridFailurePolygonSuccess in response to getGridFailurePolygon', () => {
    apiResponse = [
      [1, 1],
      [5, 5],
    ];
    spyOn(apiClient, 'getGridFailurePolygon').and.returnValue(of(apiResponse));
    effects.getGridFailurePolygon$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailurePolygonSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadGridFailurePolygon({ payload: ['xx', 'xx'] }));
  });

  it('should equal loadGridFailurePolygon Fail after loadGridFailurePolygon Error', () => {
    spyOn(apiClient, 'getGridFailurePolygon').and.returnValue(throwError('x'));
    effects.getGridFailurePolygon$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadGridFailurePolygonFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadGridFailurePolygon({ payload: ['xx', 'xx'] }));
  });

  it('should equal loadFailureReminderSuccess in response to loadFailureReminder', () => {
    apiResponse = true;
    spyOn(apiClient, 'getFailureReminder').and.returnValue(of(apiResponse));
    effects.getFailureReminder$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadFailureReminderSuccess({ payload: apiResponse }));
    });

    actions$.next(gridFailureActions.loadFailureReminder());
  });

  it('should equal loadFailureReminder Fail after loadFailureReminder Error', () => {
    spyOn(apiClient, 'getFailureReminder').and.returnValue(throwError('x'));
    effects.getFailureReminder$.pipe(take(1)).subscribe(result => {
      expect(result).toEqual(gridFailureActions.loadFailureReminderFail({ payload: 'x' }));
    });

    actions$.next(gridFailureActions.loadFailureReminder());
  });
});
