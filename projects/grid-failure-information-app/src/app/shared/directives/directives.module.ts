/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AutoResizeColumnsDirective } from '@grid-failure-information-app/shared/directives/agGrid/auto-resize-columns.directive';
import { ServerSideDirective } from '@grid-failure-information-app/shared/directives/agGrid/server-side.directive';
import { TranslateColumnDefinitionsDirective } from '@grid-failure-information-app/shared/directives/agGrid/translate-column-definitions.directive';
import { VisibleByRightDirective } from '@grid-failure-information-app/shared/directives/visible-by-right';
import { AgGridAngular } from 'ag-grid-angular';
import { DateTimePickerViewAdapterDirective } from '@grid-failure-information-app/shared/directives/date-time-picker-view-adapter.directive';
import { FormDisableDirective } from '@grid-failure-information-app/shared/directives/forms/form-disable.directive';
import { FormValidatorDirective } from '@grid-failure-information-app/shared/directives/forms/form-validator.directive';
import { VisibleByDependentFieldDirective } from '@grid-failure-information-app/shared/directives/forms/visible-by-dependent-fields.directive';
@NgModule({
    imports: [CommonModule],
    declarations: [
        AutoResizeColumnsDirective,
        TranslateColumnDefinitionsDirective,
        ServerSideDirective,
        VisibleByRightDirective,
        FormDisableDirective,
        DateTimePickerViewAdapterDirective,
        FormValidatorDirective,
        VisibleByDependentFieldDirective,
    ],
    exports: [
        AutoResizeColumnsDirective,
        TranslateColumnDefinitionsDirective,
        ServerSideDirective,
        VisibleByRightDirective,
        FormDisableDirective,
        DateTimePickerViewAdapterDirective,
        FormValidatorDirective,
        VisibleByDependentFieldDirective,
    ]
})
export class DirectivesModule {}
