/********************************************************************************
 * Copyright (c) 2020 Contributors to the Eclipse Foundation
 *
 * See the NOTICE file(s) distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { FormValidatorDirective } from '@grid-failure-information-app/shared/directives/forms/form-validator.directive';

describe('FormValidator.DirectiveDirective', () => {
  let directive: FormValidatorDirective;

  beforeEach(() => {
    directive = new FormValidatorDirective();
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });

  it('should set invalid property correctly', () => {
    directive.required = true;
    directive.ngrxFormControlState = { isDisabled: true, value: null } as any;
    expect(directive.invalid).toBeTruthy();
  });

  it('should set valid property correctly', () => {
    directive.required = true;
    directive.ngrxFormControlState = { isDisabled: true, value: 'x' } as any;
    expect(directive.valid).toBeTruthy();
  });
});
